import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Launch from './components/Launch';
import Register from './components/Register';
import Login from './components/Login';
import Login2 from './components/Login2';
import Login3 from './components/Login3';
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStackStyleInterpolator';
import {
    Scene,
    Router,
    Actions,
    Reducer,
    ActionConst,
    Overlay,
    Tabs,
    Modal,
    Drawer,
    Stack,
    Lightbox,
} from 'react-native-router-flux';
import Home from './components/Home';
import DrawerContent from './components/drawer/DrawerContent';
import TabView from './components/TabView';
import TabIcon from './components/TabIcon';
import EchoView from './components/EchoView';
import MessageBar from './components/MessageBar';
import ErrorModal from './components/modal/ErrorModal';
import DemoLightbox from './components/lightbox/DemoLightbox';
import MenuIcon from './assets/images/menu32X32.png';
// import MenuIcon from './images/menu_burger.png';

import CustomNavBarView from "./components/CustomNavBarView";
import CustomNavBar from "./components/CustomNavBar";
import CustomNavBar2 from "./components/CustomNavBar2";

import {compose, createStore, applyMiddleware} from 'redux'
import {createEpicMiddleware} from 'redux-observable'
import rootReducer from './js/reducers/rootReducer'
import {AsyncStorage} from 'react-native';
import persistState, {mergePersistedState} from 'redux-localstorage';
import adapter from 'redux-localstorage/lib/adapters/AsyncStorage';
import rootEpic from './js/epics/rootEpic'
import SplashContainer from './js/containers/SplashContainer'
import LoginContainer from './js/containers/LoginContainer'
import RechargeCellFrequentContainer from './js/containers/RechargeCellFrequentContainer'
import RechargeCellContainer from './js/containers/RechargeCellContainer'
import RechargeCellStep1Container from './js/containers/RechargeCellStep1Container'
import RechargeCellStep2Container from './js/containers/RechargeCellStep2Container'
import RechargeCellStep3Container from './js/containers/RechargeCellStep3Container'
import RechargeCellStep4Container from './js/containers/RechargeCellStep4Container'
import {Provider} from 'react-redux'
import {COLORS} from './js/constants/StylesConstants'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
    },
    tabBarStyle: {
        backgroundColor: '#fff',
    },
    tabBarSelectedItemStyle: {
        backgroundColor: '#fff',
    },
});

const reducerCreate = params => {
    const defaultReducer = new Reducer(params);
    return (state, action) => {
        console.log('ACTION:', action);
        return defaultReducer(state, action);
    };
};

const getSceneStyle = () => ({
    backgroundColor: '#F5FCFF',
    shadowOpacity: 1,
    shadowRadius: 3,
});


const epicMiddleware = createEpicMiddleware(rootEpic);
const reducer = compose(
    mergePersistedState()
)(rootReducer);
const storage = adapter(AsyncStorage);
const enhancer = compose(
    applyMiddleware(epicMiddleware),
    persistState(storage, 'recargaRapidoReduxState')
);

const defaultState = {
    login: {
        email: "",
        hasInvalidEmailOrPassword: false,
        isVisibleLoadingLogin: false,
        hasInvalidEmail: false
    },
    profile: {
        name: "",
        phone: "",
        city: "",
        country: "",
        email: "",
        dealerId: 0,
        dealerName: "",
        idToken: "",
        accessToken: "",
        userId: "",
        isVisibleLoading: false,
        deviceToken: ""
    }
};

store = createStore(
    reducer, defaultState, enhancer
);

const Example = () => (

    <Provider store={store}>
        <Router createReducer={reducerCreate} getSceneStyle={getSceneStyle}>
            <Overlay>
                <Modal
                    hideNavBar
                    transitionConfig={() => ({screenInterpolator: CardStackStyleInterpolator.forFadeFromBottomAndroid})}
                >
                    <Lightbox>
                        <Stack
                            hideNavBar
                            key="root"
                            titleStyle={{alignSelf: 'center'}}
                        >
                            <Scene key="SplashContainer" component={SplashContainer} title="SplashContainer" initial/>
                            <Scene key="LoginContainer" component={LoginContainer} title="LoginContainer"/>

                            <Drawer
                                hideNavBar
                                key="drawer"
                                contentComponent={DrawerContent}
                                drawerImage={MenuIcon}
                            >

                                <Scene hideNavBar>
                                    <Tabs
                                        key="tabbar"
                                        swipeEnabled
                                        showLabel={false}
                                        tabBarStyle={styles.tabBarStyle}
                                        activeBackgroundColor="white"
                                        inactiveBackgroundColor="white">
                                        <Stack key="tab_3">
                                            <Scene
                                                hideNavBar
                                                key="RechargeCellContainer"
                                                component={RechargeCellContainer}
                                                imageActive={require("./assets/images/smartphone_selected.png")}
                                                imageInactive={require("./assets/images/smartphone_unselected.png")}
                                                title="Celular"
                                                icon={TabIcon}
                                            />

                                            <Scene
                                                hideNavBar
                                                key="RechargeCellFrequentContainer"
                                                component={RechargeCellFrequentContainer}
                                                imageActive={require("./assets/images/smartphone_selected.png")}
                                                imageInactive={require("./assets/images/smartphone_unselected.png")}
                                                title="Celular"
                                                icon={TabIcon}
                                            />

                                            <Scene
                                                hideNavBar
                                                key="RechargeCellStep1Container"
                                                component={RechargeCellStep1Container}
                                                imageActive={require("./assets/images/smartphone_selected.png")}
                                                imageInactive={require("./assets/images/smartphone_unselected.png")}
                                                title="Celular"
                                                icon={TabIcon}
                                            />

                                            <Scene
                                                hideNavBar
                                                key="RechargeCellStep2Container"
                                                component={RechargeCellStep2Container}
                                                imageActive={require("./assets/images/smartphone_selected.png")}
                                                imageInactive={require("./assets/images/smartphone_unselected.png")}
                                                title="Celular"
                                                icon={TabIcon}
                                            />

                                            <Scene
                                                hideNavBar
                                                key="RechargeCellStep3Container"
                                                component={RechargeCellStep3Container}
                                                imageActive={require("./assets/images/smartphone_selected.png")}
                                                imageInactive={require("./assets/images/smartphone_unselected.png")}
                                                title="Celular"
                                                icon={TabIcon}
                                            />

                                            <Scene
                                                hideNavBar
                                                key="RechargeCellStep4Container"
                                                component={RechargeCellStep4Container}
                                                imageActive={require("./assets/images/smartphone_selected.png")}
                                                imageInactive={require("./assets/images/smartphone_unselected.png")}
                                                title="Celular"
                                                icon={TabIcon}
                                            />
                                        </Stack>
                                        <Stack key="tab_4">
                                            <Scene key="tab_4_1"
                                                   hideNavBar
                                                   component={TabView}
                                                   imageActive={require("./assets/images/wifi_selected.png")}
                                                   imageInactive={require("./assets/images/wifi_unselected.png")}
                                                   title="Nauta"
                                                   icon={TabIcon}/>
                                        </Stack>
                                        <Stack key="tab_5">
                                            <Scene key="tab_5_1"
                                                   hideNavBar
                                                   component={TabView}
                                                   imageActive={require("./assets/images/smartphone_call_selected.png")}
                                                   imageInactive={require("./assets/images/smartphone_call_unselected.png")}
                                                   title="Llamar"
                                                   icon={TabIcon}/>
                                        </Stack>
                                    </Tabs>
                                </Scene>
                            </Drawer>
                        </Stack>
                    </Lightbox>
                </Modal>
            </Overlay>
        </Router>
    </Provider>
);

export default Example;
