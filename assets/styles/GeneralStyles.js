import {StyleSheet} from 'react-native'

export default StyleSheet.create({
    tabBarStyle: {
        backgroundColor: '#fff',
        borderTopWidth: 1,
        borderTopColor: '#e6e6ed',
        height: 60,
        bottom: 0,
        position: 'relative'
    },
    navBarStyle: {
        shadowOpacity: 0.2,
        shadowRadius: 1,
        shadowOffset: {height: 1, width: 0}
    },
    // fontRalewaySemibold: {
    //     fontFamily: 'raleway-semibold'
    // },
    // fontRalewayBold: {
    //     fontFamily: 'raleway-bold'
    // },
    // fontRalewayRegular: {
    //     fontFamily: 'raleway-regular'
    // },
    // fontLatoRegular: {
    //     fontFamily: 'lato-regular'
    // },
    // fontLatoBold: {
    //     fontFamily: 'lato-bold'
    // },

})