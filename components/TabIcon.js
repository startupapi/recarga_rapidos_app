import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
} from 'react-native';
import PropTypes from "react";
import {COLORS} from '../js/constants/StylesConstants'

const propTypes = {
};

const TabIcon = (props) => {

    let styleText = props.focused ? styles.textActive : styles.textInactive
    return (
        <View style={[styles.containerMain]}>
            <Image style={[styles.icon]} source={props.focused ? props.imageActive : props.imageInactive}/>
            <Text
                style={[styles.text, styleText]}>{props.title.toUpperCase()}</Text>
        </View>
    );

};

const styles = StyleSheet.create({
    containerMain: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 5,
        paddingBottom: 5,
    },
    icon: {
        width: 24,
        height: 24,
        marginBottom: 3,
    },
    text: {
        fontSize: 12
    },
    textActive: {
        color: COLORS.BLUE_COLOR
    },
    textInactive: {
        color: 'black'
    }
});

TabIcon.propTypes = propTypes;

export default TabIcon;
