const VALID_EMAIL_PATERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const VALID_NUMBER_PATERN = /^\d+$/;

function isEmpty(str){
    return !str || String(str).replace(/ /g,'').length === 0
}
function isValidEmail(email) {
    return !VALID_EMAIL_PATERN.test(email)
}
function isValidNumber(number) {
    return !VALID_NUMBER_PATERN.test(number)
}
function isValidIdentification(identification){
    identification = identification.toString();
    if(identification.length !== 10){
        return true;
    }
    var regionNumber = identification.substring(0,2);
    if( !(regionNumber >= 1 && regionNumber <=24) ){
        return true;
    }
    const sumDigits = identification.substring(0,9).split('')
        .map((digit, index) => (index + 1)%2 == 0?digit*1:digit*2)
        .map(digit => digit>9 ? digit-9 : digit)
        .reduce((acc, digit) => acc + digit,0);
    const modul10 = sumDigits%10;
    const verifier = (modul10 === 0 ? 0 : (10 - modul10));
    return verifier !== parseInt(identification.charAt(9));
}
module.exports = {
    isEmpty: isEmpty,
    isValidEmail: isValidEmail,
    isValidNumber: isValidNumber,
    isValidIdentification: isValidIdentification
};