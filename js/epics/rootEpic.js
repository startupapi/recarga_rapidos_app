import {combineEpics} from 'redux-observable'
import * as LoginEpics from './LoginEpics'
import * as RechargeCellEpics from './RechargeCellEpics'
import * as RechargeCellFrequentEpics from './RechargeCellFrequentEpics.js'
import * as GenericEpics from './GenericEpics'

const rootEpic = combineEpics(
    LoginEpics.loginEpic,
    RechargeCellEpics.goToRechargesFrequentEpic,
    GenericEpics.goToLateralMenu,
    GenericEpics.goToBack,
    RechargeCellFrequentEpics.onPressRecharge,
);


export default rootEpic