import * as LoginActions from '../actions/LoginActions'
import * as ProfileActions from '../actions/ProfileActions'
import {mergeMap} from 'rxjs'
import {ajax} from 'rxjs/observable/dom/ajax'
import {Observable} from 'rxjs/Observable'
import {fromPromise} from 'rxjs/observable/fromPromise'
import {Actions} from 'react-native-router-flux';

export const loginEpic = action$ =>
    action$.ofType(LoginActions.LOGIN)
        .mergeMap(action => {
                Actions.drawer();
            return Observable.of({
                    type: LoginActions.LOGGED,
                    accessToken: "1111",
                    idToken: "1111",
                    deviceToken: "1111"
                })
            }
        );
