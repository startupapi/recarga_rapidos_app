import * as GenericActions from '../actions/GenericActions'
import {Actions} from 'react-native-router-flux';

export const goToLateralMenu = action$ =>
    action$.ofType(GenericActions.ON_PRESS_OPEN_MENU)
        .forEach(action => {
            Actions.drawerOpen()
        });

export const goToBack = action$ =>
    action$.ofType(GenericActions.ON_PRESS_BACK)
        .forEach(action => {
            Actions.pop()
        });