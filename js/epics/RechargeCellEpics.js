import * as RechargeCellActions from '../actions/RechargeCellActions'
import {mergeMap} from 'rxjs'
import {Actions} from 'react-native-router-flux';

export const goToRechargesFrequentEpic = action$ =>
    action$.ofType(RechargeCellActions.ON_PRESS_RECHARGES_FREQUENT)
        .forEach(action => {
            Actions.RechargeCellFrequentContainer()
        });