import * as RechargeCellFrequentActions from '../actions/RechargeCellFrequentActions'
import {Actions} from 'react-native-router-flux';

export const onPressRecharge = action$ =>
    action$.ofType(RechargeCellFrequentActions.ON_PRESS_RECHARGE)
        .forEach(action => {
            Actions.RechargeCellStep1Container()
        });