export const GO_TO_EDIT_PROFILE = "GO_TO_EDIT_PROFILE";
export const GO_TO_PROFILE = "GO_TO_PROFILE";
export const GO_TO_PREFERENCES = "GO_TO_PREFERENCES";
export const CALL_CONCIERGE = "CALL_CONCIERGE";
export const GO_TO_CHANGE_PASSWORD = "GO_TO_CHANGE_PASSWORD";
export const GO_TO_SEND_FEEDBACK = "GO_TO_SEND_FEEDBACK";
export const GO_TO_ABOUT = "GO_TO_ABOUT";
export const NAME_GOT = "NAME_GOT";
export const USER_SUB_GOT = "USER_SUB_GOT";
export const GET_INFO = "GET_INFO";
export const GET_USER_SUB = "GET_USER_SUB";
export const GO_TO_SETTINGS = 'GO_TO_SETTINGS';
export const SEND_FEEDBACK = 'SEND_FEEDBACK';
export const FEEDBACK_SENT = 'FEEDBACK_SENT';
