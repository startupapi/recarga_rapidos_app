import * as ProfileActions from '../actions/ProfileActions'
import * as LoginActions from '../actions/LoginActions'
import * as EditProfileActions from '../actions/EditProfileActions'
import {Actions} from 'react-native-router-flux';
const profile = (state = {}, action) => {

    switch (action.type) {
        case ProfileActions.NAME_GOT:
            return {
                ...state,
                email: action.email,
                phone: action.phone,
                city: action.city,
                country: action.country,
                name: action.name,
                dealerId: action.dealerId,
                dealerName: action.dealerName,
                idToken: action.idToken,
                userId: action.userId
            };
        case LoginActions.LOGGED:
            return {
                ...state,
                idToken: action.idToken
            };
        case EditProfileActions.SAVE_PROFILE :
            return {
                ...state,
                isVisibleLoading: true
            };
        case EditProfileActions.SAVED_PROFILE :
            return {
                ...state,
                isVisibleLoading: false,
                email: action.email,
                phone: action.phone,
                name: action.name,
            };
        case EditProfileActions.FAIL_SAVED_PROFILE :
            return {
                ...state,
                isVisibleLoading: false,
            };
        case LoginActions.LOGGED_OUT:
            return {
                ...state,
                email: action.email,
                phone: action.phone,
                city: action.city,
                country: action.country,
                name: action.name,
                dealerId: action.dealerId,
                dealerName: action.dealerName,
                idToken: action.idToken,
            };
        case "GOT_DEVICE_TOKEN" : {
            return {
                ...state,
                deviceToken: action.deviceToken
            }
        }
        default:
            return state
    }
};

export default profile