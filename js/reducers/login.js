import * as LoginActions from '../actions/LoginActions'

const login = (state = {}, action) => {
    
    switch (action.type) {

        case LoginActions.LOGIN:
            return {
                ...state,
                isVisibleLoadingLogin: true,
            };

        case LoginActions.LOGGED:
            return {
                ...state,
                hasInvalidEmailOrPassword: false,
                isVisibleLoadingLogin: false,
            };
        case LoginActions.LOGIN_FAILED:
            return {
                ...state,
                hasInvalidEmailOrPassword: true,
                isVisibleLoadingLogin: false,
            };
        case  LoginActions.ON_CHANGE_EMAIL :
            return {
                ...state,
                hasInvalidEmailOrPassword: false,
            };
        case  LoginActions.ON_CHANGE_PASSWORD :
            return {
                ...state,
                hasInvalidEmailOrPassword: false,
            };
        case LoginActions.CHANGED_PASSWORD:
            return {
                ...state,
                hasInvalidEmail: false,
            };
        case LoginActions.CHANGE_PASSWORD_FAILED:
            return {
                ...state,
                hasInvalidEmail: true,
            };
        default:
            return {
                ...state, hasInvalidPassword: false,
                hasInvalidEmail: false
            }
    }

};

export default login