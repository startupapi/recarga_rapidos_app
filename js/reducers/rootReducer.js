import {combineReducers} from 'redux'
import profile from './profile'
import login from './login'
// import notification from './notification'
// import catalogSearch from './catalogSearch'
// import calculator from './calculator'
// import loanApplication from './loanApplication'
// import uiReducer from './ui/uiReducer'
// import businessOpportunity from './businessOpportunity'
// import businessOpportunitiesSearchModal from './businessOpportunitiesSearchModal'
// import loanApplicationSearchModal from './loanApplicationSearchModal'

const rootReducer = combineReducers({
    profile,
    login,
    // notification,
    // catalogSearch,
    // calculator,
    // loanApplication,
    // businessOpportunity,
    // businessOpportunitiesSearchModal,
    // loanApplicationSearchModal,
    // ui: uiReducer
});

export default rootReducer