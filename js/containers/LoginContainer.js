import {connect} from 'react-redux'
import LoginView from '../components/views/LoginView'
import * as LoginActions from '../actions/LoginActions'

function mapStateToProps(state) {
    return {
        ...state,
        email: state.login.email,
        showPassword: state.login.showPassword,
        isVisibleLoadingLogin: false,//state.login.isVisibleLoadingLogin,
        hasInvalidEmailOrPassword: state.login.hasInvalidEmailOrPassword,
    }
}
function mapDispatchToProps(dispatch) {

    return {
        onPressLoginDispatch: (deviceToken) => (email, password) => () => {
            console.log("Devicetoken: " , deviceToken)
            dispatch({
                type: LoginActions.LOGIN,
                email,
                password,
                deviceToken
            });
        },
        onPressForgotPassword: () => {
            dispatch({
                type: LoginActions.GO_TO_FORGOT_PASSWORD,
            });
        },
        onChangeEmail: () => {
            dispatch({
                type: LoginActions.ON_CHANGE_EMAIL,
            });
        },
        onChangePassword: () => {
            dispatch({
                type: LoginActions.ON_CHANGE_PASSWORD,
            });
        },
    }
}

function mergeProps(stateProps, dispatchProps) {
    return {
        ...stateProps,
        ...dispatchProps,
        onPressLogin: dispatchProps.onPressLoginDispatch(stateProps.profile.deviceToken)
    }
}

export default connect(mapStateToProps, mapDispatchToProps,mergeProps)(LoginView)

