import {connect} from 'react-redux'
import RechargeCellView from '../components/views/RechargeCellView'
import * as RechargeCellActions from "../actions/RechargeCellActions";
import * as GenericActions from "../actions/GenericActions";

function mapStateToProps(state) {
    return state
}

function mapDispatchToProps(dispatch) {
    return {
        onOpenMenu: () => {
            dispatch({
                type: GenericActions.ON_PRESS_OPEN_MENU,
            });
        },
        onPressRechargesFrequent: () => {
            dispatch({
                type: RechargeCellActions.ON_PRESS_RECHARGES_FREQUENT,
            });
        }
    }
}

function mergeProps(stateProps, dispatchProps) {
    return {
        ...stateProps,
        ...dispatchProps,
        onOpenMenu: dispatchProps.onOpenMenu,
        onPressRechargesFrequent: dispatchProps.onPressRechargesFrequent,
    }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(RechargeCellView)

