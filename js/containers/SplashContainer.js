import {connect} from 'react-redux'
import SplashView from '../components/views/SplashView'
import {Actions} from 'react-native-router-flux';
// import PlatformUtil from '../helpers/PlatformUtil'

function mapStateToProps(state) {
    return state
}

function mapDispatchToProps(dispatch) {
    return {
        verifyUserlogin: (profile) => () => {
            if (profile && profile.idToken && profile.idToken !== "") {

                console.log("111111111111")
                // Actions.Home();
                // dispatch({
                //     type: "INIT_APP",
                //     userId: profile.userId
                // })
                Actions.drawer()
            } else {
                console.log("2222222222222222")
                // PlatformUtil.getDeviceToken((token) => {
                //     dispatch({
                //         type: "GOT_DEVICE_TOKEN",
                //         deviceToken: token
                //     })
                // })
                Actions.LoginContainer()
            }
        },
    }
}

function mergeProps(stateProps, dispatchProps) {
    return {
        ...stateProps,
        ...dispatchProps,
        onSplashEnd: dispatchProps.verifyUserlogin(stateProps.profile),
    }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(SplashView)

