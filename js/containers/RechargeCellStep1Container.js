import {connect} from 'react-redux'
import RechargeCellStep1View from '../components/views/RechargeCellStep1View'
import * as GenericActions from "../actions/GenericActions";

function mapStateToProps(state) {
    return state
}

function mapDispatchToProps(dispatch) {
    return {
        onPressBack: () => {
            dispatch({
                type: GenericActions.ON_PRESS_BACK,
            });
        },
    }
}

function mergeProps(stateProps, dispatchProps) {
    return {
        ...stateProps,
        ...dispatchProps,
    }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(RechargeCellStep1View)

