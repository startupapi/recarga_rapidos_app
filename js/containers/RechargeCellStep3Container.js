import {connect} from 'react-redux'
import RechargeCellStep3View from '../components/views/RechargeCellStep3View'
import * as GenericActions from "../actions/GenericActions";

function mapStateToProps(state) {
    return state
}

function mapDispatchToProps(dispatch) {
    return {
        onPressBack: () => {
            dispatch({
                type: GenericActions.ON_PRESS_BACK,
            });
        },
    }
}

function mergeProps(stateProps, dispatchProps) {
    return {
        ...stateProps,
        ...dispatchProps,
    }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(RechargeCellStep3View)

