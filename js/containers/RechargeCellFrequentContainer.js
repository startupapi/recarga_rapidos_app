import {connect} from 'react-redux'
import RechargeCellFrequentView from '../components/views/RechargeCellFrequentView'
import * as GenericActions from "../actions/GenericActions";
import * as RechargeCellFrequentActions from "../actions/RechargeCellFrequentActions";

function mapStateToProps(state) {
    return state
}

function mapDispatchToProps(dispatch) {
    return {
        onPressBack: () => {
            dispatch({
                type: GenericActions.ON_PRESS_BACK,
            });
        },
        onPressRecharge: () => {
            dispatch({
                type: RechargeCellFrequentActions.ON_PRESS_RECHARGE,
            });
        },
    }
}

function mergeProps(stateProps, dispatchProps) {
    return {
        ...stateProps,
        ...dispatchProps,
    }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(RechargeCellFrequentView)

