const GLOBAL = {
    EMAIL: 'correo electrónico',
    PASSWORD: 'contraseña',
};

const LOGIN = {
    START_SESSION: 'Inicia sesión',
    BUTTON_START_SESSION: 'Iniciar sesión',
    FORGET_PASSWORD: '¿Olvidaste la contraseña?',
    FORGET_PASSWORD_TITLE: '¿Olvidaste tu contraseña?',
    FORGET_PASSWORD_SUBTITLE: 'Introduce tu correo electrónico para reiniciar tu contraseña.',
    RESET_PASSWORD: 'Reiniciar contraseña',
    NEW_ACCOUNT: 'Crear nueva cuenta',
};

const labels = ["Orden", "Resumen", "Pago", "Fin"];
const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#00a65a',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#00a65a',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#00a65a',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#00a65a',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#00a65a',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#00a65a'
};

module.exports = {
    LABELS: labels,
    CUSTOM_STYLES: customStyles
};