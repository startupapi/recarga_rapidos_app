const GLOBAL = {
    EMAIL: 'correo electrónico',
    PASSWORD: 'contraseña',
};

const LOGIN = {
    START_SESSION: 'Inicia sesión',
    BUTTON_START_SESSION: 'Iniciar sesión',
    FORGET_PASSWORD: '¿Olvidaste la contraseña?',
    FORGET_PASSWORD_TITLE: '¿Olvidaste tu contraseña?',
    FORGET_PASSWORD_SUBTITLE: 'Introduce tu correo electrónico para reiniciar tu contraseña.',
    RESET_PASSWORD: 'Reiniciar contraseña',
    NEW_ACCOUNT: 'Crear nueva cuenta',
};

module.exports = {
    GLOBAL,
    LOGIN
};