import {
    Platform
} from 'react-native';
const COLORS = {
    BLACK: '#000000',
    WHITE: '#ffffff',
    // PRIMARY_COLOR: '#0081e8',
    // PRIMARY_LIGHT_COLOR: '#79cdf6',
    // SECONDARY_COLOR: '#52535a',
    TEXT_COLOR: '#333337',
    // TEXT_LIGHTEEN_1: '#72727d',
    // TEXT_LIGHTEEN_2: '#b3b4bc',
    TEXT_LIGHTEEN_3: '#b3b3bb',
    TEXT_LIGHTEEN_4: '#72727c',
    BORDER_COLOR: '#e6e6ed',
    LIGHT_BG: '#eaeaed',
    // SUCCESS_COLOR: '#64b700',
    // WARNING_COLOR: '#ff604b',
    ALERT_COLOR: '#eb212e',
    // SWITCH_COLOR: '#7de501',
    BLUE_COLOR: '#42A5F5',
    BLUE_COLOR_DARK: '#3c8dbc',
    BLUE_COLOR_LIGHTEEN: '#00c0ef',
    BLUE_COLOR_BORDER: '#0097bc',
    GREEN_COLOR: '#00a65a',
    // PURPLE_LIGHT_COLOR: '#7859ab67',
    // PURPLE_COLOR: '#7859ab',
    // SWIPE_GRAY: '#93939c',
    // SWIPE_ORANGE: '#e56901',
    // ALERT_LABEL: '#e56901',
    ALERT_BORDER: '#eb212e',
    // LIGHT_BLUE_COLOR: '#00A6A2',
};

const Size1 = 30;
const Size2 = 18;
const Size3 = 15;
const Size4 = 12;
const Size5 = 9;

const FONTS = {
    FONT_BASE: Size2,
    SIZE_TITLE: Size1,
    SIZE_SMALL: Size4,
    SIZE_NAV: Size5,
    SIZE_CAPITALIZE: Size3,
    TEXT_THIN: '300',
    TEXT_REGULAR: '500',
    TEXT_BOLD: '700',
    TEXT_SEMI_BOLD: '600',
    // DEFAULT_TEXT: {
    //     fontSize: Size2,
    //     fontFamily: 'Lato'
    // },
    // TITLE_TEXT: {
    //     fontSize: Size1,
    //     fontFamily: 'Raleway'
    // },
    // PRIMARY_TEXT: {
    //     fontFamily: 'Raleway'
    // },
    // SECONDARY_TEXT: {
    //     fontFamily: 'Lato'
    // },
    // SMALL_TEXT: {
    //     fontSize: Size4,
    //     fontFamily: 'Raleway'
    // },
    // EXTRA_SMALL_TEXT: {
    //     fontSize: Size5,
    //     fontFamily: 'Raleway'
    // },
    // CAPITALIZE_TEXT: {
    //     fontSize: Size3,
    //     fontFamily: 'Raleway'
    // },
    // LABEL_TEXT: {
    //     fontSize: Size5,
    //     fontFamily: 'Raleway'
    // },
    // MIDDLE_TEXT: {
    //     fontSize: Size2,
    //     fontFamily: 'Lato'
    // },
    // fontRalewaySemibold: {
    //     fontFamily: 'raleway-semibold'
    // },
    // fontRalewayBold: {
    //     fontFamily: 'raleway-bold'
    // },
    // fontRalewayRegular: {
    //     fontFamily: 'raleway-regular'
    // },
    // fontLatoRegular: {
    //     fontFamily: 'lato-regular'
    // },
    // fontLatoBold: {
    //     fontFamily: 'lato-bold'
    // }
};


const DISPLAY = {
    MARGIN_EXTRA_LARGE: 40,
    MARGIN_LARGE: 30,
    MARGIN_DEFAULT: 20,
    MARGIN_SMALL: 10,
    MARGIN_EXTRA_SMALL: 5,
    NAV_BAR_PADDING: 	{...Platform.select({
        ios: {
            paddingTop: 65
        },
        android: {
            paddingTop: 45
        }
    })},
    NO_NAV_BAR_PADDING: 	{...Platform.select({
        ios: {
            paddingTop: 20
        },
        android: {
            paddingTop: 0
        }
    })}
};

const BUTTON = {
    SMALL_RADIUS: 50,
    SMALL_PADDING: 10,
    LARGE_PADDING: 15
};

const ICON = {
    MENU: require('../../assets/images/menu.png'),
    BACK: require('../../assets/images/left-arrow.png'),
    SUCCESS_CHECK: require('../../assets/images/success_check.png')
};


module.exports = {
    COLORS,
    FONTS,
    DISPLAY,
    ICON,
    BUTTON
};