export const BLUE = "BLUE";
export const GREEN = "GREEN";
export const PURPLE = "PURPLE";
export const PURPLE_LIGHT = "PURPLE_LIGHT";
export const WHITE = "WHITE";
export const GRAY = "GRAY";
export const FLAT = "FLAT";