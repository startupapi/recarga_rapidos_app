import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity, Platform, Image} from 'react-native';
import PropTypes from 'prop-types';
import TextBoxType from "./TextBoxType"
import {isValidEmail, isEmpty, isValidNumber, isValidIdentification} from "../../helpers/ValidationUtils"
import * as ValidateConstant from "../../constants/ValidateConstant"
import {COLORS, FONTS, ICON} from '../../constants/StylesConstants'

class TextBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showPass: true,
            text: this.props.value,
            remainingCharacters: this.props.maxLength,
            showRemainingCharacters: this.props.showRemainingCharacters && this.props.maxLength ? true : false
        };
        this.onChangeText = this.onChangeText.bind(this);
        this.onPressShowPassword = this.onPressShowPassword.bind(this);
    }

    onPressShowPassword() {
        this.setState({showPass: !this.state.showPass})
    }

    onChangeText(text) {

        this.setState({text, remainingCharacters: this.props.maxLength - text.length});
        if (this.props.required && isEmpty(text)) {
            this.props.onChangeText(text, ValidateConstant.ERROR_REQUIRED);
        } else {
            switch (this.props.type) {
                case TextBoxType.EMAIL:
                    isValidEmail(text) ? this.props.onChangeText(text, ValidateConstant.ERROR_EMAIL_FORMAT) : this.props.onChangeText(text, null);
                    break;
                case TextBoxType.DOC_NUMBER:
                    isValidNumber(text) ? this.props.onChangeText(text, ValidateConstant.ERROR_NUMBER_FORMAT) : isValidIdentification(text) ? this.props.onChangeText(text, ValidateConstant.ERROR_ID_FORMAT) : this.props.onChangeText(text, null);
                    break;
                case TextBoxType.NUMBER:
                    isValidNumber(text) ? this.props.onChangeText(text, ValidateConstant.ERROR_NUMBER_FORMAT) : this.props.onChangeText(text, null);
                    break;
                default:
                    this.props.onChangeText(text, null);
            }
        }
    }

    render() {

        let labelShow = null;
        let iconError = null;
        let labelError = null;

        let labelRequiredAst = this.props.required ? <Text style={styles.labelRequiredAst}>*</Text> : null;

        const labelText = <Text
            style={[{marginTop: 7}, styles.label, FONTS.fontRalewaySemibold, this.props.hasError && this.props.labelError !== null ? styles.labelInputError : null, {color: this.props.labelColorStyle}]}>{this.props.label.toUpperCase()} {labelRequiredAst} </Text>;


        if (this.props.hasError) {
            labelError = this.props.labelError !== null ? <Text
                style={[styles.labelError, FONTS.fontRalewayBold]}>{this.props.labelError.toUpperCase()}</Text> : null;
        }

        if (this.props.type === TextBoxType.PASSWORD) {
            labelShow = <TouchableOpacity onPress={this.onPressShowPassword}>
                <Text style={[styles.label, styles.labelShow, FONTS.fontRalewaySemibold]}>MOSTRAR</Text>
            </TouchableOpacity>
        } else if (this.props.type === TextBoxType.CUPON) {
            labelShow =
                <TouchableOpacity style={styles.labelCuponContainer} onPress={this.props.onPressShowValidateCupon}>
                    <Text
                        style={[styles.label, styles.labelCupon, FONTS.fontRalewaySemibold]}> APLICAR </Text>
                </TouchableOpacity>
        }

        if (this.props.hasError && this.props.type !== TextBoxType.PASSWORD && this.props.labelError !== null && !this.props.hideWarningIcon) {
            iconError = <Image source={ICON.ALERT_LABEL}/>;
        }

        let labelTextContainer = <View style={styles.labelTextContainer}>
            {labelText}
            {iconError}
            {labelShow}
        </View>;

        let keyboardType = 'default';
        switch (this.props.type) {
            case TextBoxType.EMAIL:
                keyboardType = 'email-address';
                break;
            case TextBoxType.NUMBER:
                keyboardType = 'numeric';
                break;
        }


        let textInput = this.props.disabled ? <Text style={[styles.textInput, FONTS.fontLatoRegular]}
                                                    onPress={this.props.onPress}>{this.state.text}</Text>
            : <TextInput
                style={[styles.textInput, FONTS.fontLatoRegular, this.props.inputStyle]}
                multiline={this.props.multiline}
                maxLength={this.props.maxLength}
                numberOfLines={this.props.numberOfLines}
                onChangeText={this.onChangeText}
                value={this.state.text.toString()}
                underlineColorAndroid='transparent'
                autoCorrect={false}
                secureTextEntry={this.props.type === TextBoxType.PASSWORD && this.state.showPass}
                placeholder={this.props.placeholder}
                autoFocus={this.props.autoFocus}
                keyboardType={keyboardType}
                onBlur={this.props.onBlur}
                onFocus={this.props.onFocus}
            />;

        let remainingCharacters = this.state.showRemainingCharacters ? <Text
            style={[FONTS.fontLatoRegular, styles.remainingCharacters]}>{this.state.remainingCharacters} caracteres
            restantes</Text> : null;

        let component = null;
        if (this.props.visible) {
            component = <View style={[styles.container, this.props.style]}>
                <View
                    style={[styles.textBoxContainer, {borderColor: this.props.borderColorStyle}, this.props.hasError && this.props.labelError !== null ? styles.textBoxContainerWithError : this.props.type === "borderLess" ? styles.textBoxContainerNoBorder : null]}>
                    {labelTextContainer}
                    {textInput}
                </View>
                {remainingCharacters}
                {labelError}
            </View>
        }

        return component;
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
    },
    textBoxContainer: {
        borderWidth: 2,
        borderRadius: 5,
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,
    },
    textBoxContainerWithError: {
        borderColor: COLORS.ALERT_BORDER,
    },
    textBoxContainerNoBorder: {
        borderColor: COLORS.WHITE,
    },
    labelTextContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    label: {
        fontSize: 12,
        letterSpacing: 2,
        alignSelf: 'flex-start',
        marginLeft: 5,
    },
    textInput: {
        height: 30,
        fontSize: 20,
        lineHeight: 30,
        color: COLORS.TEXT_COLOR,
        marginTop: 4,
        ...Platform.select({
            android: {height: 44}
        })
    },
    labelInputError: {
        color: COLORS.ALERT_COLOR,
    },
    labelError: {
        color: COLORS.ALERT_COLOR,
        letterSpacing: 1.5,
        fontSize: 10,
        marginTop: 4,
        textAlign: "left",
        marginRight: -2
    },
    labelShow: {
        alignSelf: 'flex-end',
        marginRight: 5
    },
    labelCupon: {
        alignSelf: 'flex-end',
        color: COLORS.WHITE,
        fontWeight: 'bold'
    },
    remainingCharacters: {
        alignSelf: 'flex-end',
        color: COLORS.TEXT_LIGHTEEN_3,
        fontSize: 12,
        marginTop: 4,
        lineHeight: 16
    },
    labelRequiredAst: {
        color: COLORS.ALERT_COLOR
    },
    labelCuponContainer: {
        borderColor: COLORS.BLUE_COLOR_DARK,
        backgroundColor: COLORS.BLUE_COLOR_DARK,
        borderWidth: 1,
        marginTop: 3,
        marginRight: 3,
        borderRadius: 3,
        padding: 3,
    }
});

TextBox.propTypes = {
    hasError: PropTypes.bool,
    labelError: PropTypes.string,
    onPressShowValidateCupon: PropTypes.func,
    onChangeText: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    required: PropTypes.bool,
    visible: PropTypes.bool
};

TextBox.defaultProps = {
    value: "",
    label: "",
    labelError: "",
    required: false,
    visible: true,
    borderColorStyle: COLORS.BORDER_COLOR,
    labelColorStyle: COLORS.TEXT_LIGHTEEN_3
}

export default TextBox