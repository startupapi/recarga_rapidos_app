import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native'
import DropdownContent from './DropdownContent';
import {COLORS, FONTS, ICON} from "../../../constants/StylesConstants";
import TextBoxType from "../TextBoxType";

class Dropdown extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };
        this.onOpenModal = this.onOpenModal.bind(this);
        this.onCloseModal = this.onCloseModal.bind(this);
        this.onPress = this.onPress.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onOpenModal() {
        this.setState({isOpen: true});
        if (this.props.onOpenModal) this.props.onOpenModal()
    }

    onCloseModal() {
        this.setState({isOpen: false});
        if (this.props.onCloseModal) this.props.onCloseModal()
    }

    onPress() {
        this.refs.modalDropdown.openModal();
    }

    onChange(item) {
        this.props.onChange(item, null)
    }

    render() {
        let labelRequiredAst = this.props.required ? <Text style={styles.labelRequiredAst}>*</Text> : null;
        let iconError = null;
        let labelError = null;

        const labelText = <Text
            style={[styles.label, FONTS.fontRalewaySemibold, this.props.hasError && this.props.labelError !== null ? styles.labelInputError : null]}>{this.props.label.toUpperCase()} {labelRequiredAst} </Text>;
        if (this.props.hasError) {
            labelError = this.props.labelError !== null ? <Text
                style={[styles.labelError, FONTS.fontRalewayBold]}>{this.props.labelError.toUpperCase()}</Text> : null;
        }
        if (this.props.hasError && this.props.type !== TextBoxType.PASSWORD && this.props.labelError !== null && !this.props.hideWarningIcon) {
            iconError = <Image source={ICON.ALERT_LABEL}/>;
        }
        let labelTextContainer = <View style={styles.labelTextContainer}>
            {labelText}
            {iconError}
        </View>;

        let dropdownContent = null;
        let dropdownImage = null;

        if (this.props.disabled === false) {
            dropdownContent = <DropdownContent
                ref="modalDropdown"
                heightRatio={this.props.heightRatio}
                options={this.props.options}
                defaultValue={this.props.text}
                onChange={this.onChange}
                onDropdownWillHide={this.onCloseModal}
                onDropdownWillShow={this.onOpenModal}
            />
            dropdownImage = <Image style={styles.orderImage}
                                   source={this.state.isOpen ? require('../../../../assets/images/up-arrow.png') : require('../../../../assets/images/down-arrow.png')}/>
        } else {
            dropdownContent = <Text style={styles.inputValue}>{this.props.text.toUpperCase()}</Text>
        }

        let component = null;
        if (this.props.visible) {
            component = <View style={[styles.container, this.props.style]}>
                <View
                    style={[styles.orderContainer, this.props.hasError && this.props.labelError !== null ? styles.textBoxContainerWithError : null]}>
                    {labelTextContainer}

                    <TouchableOpacity style={styles.orderButton}
                                      onPress={this.props.disabled === false ? this.onPress : null}>
                        {dropdownContent}
                        {dropdownImage}
                    </TouchableOpacity>

                </View>
                {labelError}
            </View>
        }


        return component;
    }
}

const styles = StyleSheet.create({
    container: {},
    label: {
        color: COLORS.TEXT_LIGHTEEN_3,
        fontSize: 12,
        letterSpacing: 2,
        alignSelf: 'flex-start',
        marginTop: 5
    },
    orderContainer: {
        height: 65,
        borderColor: COLORS.BORDER_COLOR,
        borderWidth: 2,
        borderRadius: 5,
        paddingLeft: 5,
        paddingRight: 5,
    },
    textBoxContainerWithError: {
        borderBottomColor: COLORS.ALERT_BORDER,
    },
    labelTextContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    dropdownContainer: {
        flex: 1,
        shadowColor: '#000',
        paddingBottom: 12,
        paddingTop: 12
    },
    itemContainer: {
        flexDirection: 'row',
        paddingTop: 12,
        paddingBottom: 12
    },
    orderButton: {
        flexDirection: 'row',
        paddingTop: 12,
        paddingBottom: 12,
        paddingLeft: 5,
        justifyContent: 'space-between'
    },
    orderBy: {
        fontSize: 11,
        lineHeight: 16,
        letterSpacing: 1.5,
        color: '#72727c'
    },
    orderDate: {
        marginLeft: 8,
        color: '#333337'
    },
    orderImage: {
        marginRight: 8,
        marginLeft: 8,
        alignSelf: 'flex-end',
        width: 24,
        height: 24,
    },
    item: {
        fontSize: 12,
        lineHeight: 16,
        letterSpacing: 1.5,
        color: '#b3b3bb',
    },
    selectedItem: {
        color: '#333337'
    },
    unselectedItem: {
        marginLeft: 28
    },
    image: {
        marginLeft: 8,
        marginRight: 8,
        marginTop: 2
    },
    labelRequiredAst: {
        color: COLORS.ALERT_COLOR
    },
    labelError: {
        color: COLORS.ALERT_COLOR,
        letterSpacing: 1.5,
        fontSize: 10,
        marginTop: 4,
        textAlign: "right",
        marginRight: -2
    },
    labelInputError: {
        color: COLORS.ALERT_COLOR,
    },
    inputValue: {
        fontSize: 16,
        lineHeight: 16,
        letterSpacing: 1.5,
        color: '#333337'
    }
});


Dropdown.propTypes = {
    label: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    options: PropTypes.array,
    onChange: PropTypes.func,
    onOpenModal: PropTypes.func,
    onCloseModal: PropTypes.func,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    visible: PropTypes.bool,
};

Dropdown.defaultProps = {
    required: false,
    disabled: false,
    visible: true
};

export default Dropdown