import React, {Component} from 'react';
import {
    StyleSheet,
    Dimensions,
    View,
    Text,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Modal,
    Image,
    ScrollView
} from 'react-native';
import PropTypes from 'prop-types';
import GeneralStyles from '../../../../assets/styles/GeneralStyles'
import {COLORS} from '../../../constants/StylesConstants'

const optionHeight = 45;

class DropdownContent extends Component {

    constructor(props) {
        super(props);
        this._button = null;
        this._buttonFrame = null;

        this.state = {
            disabled: props.disabled,
            accessible: props.accessible !== false,
            loading: props.options === null,
            showDropdown: false,
            buttonText: props.defaultValue,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            disabled: nextProps.disabled,
            buttonText: nextProps.defaultValue,
        });
    }

    render() {
        return (
            <View {...this.props}>
                {this._renderButton()}
                {this._renderModal()}
            </View>
        );
    }

    _updatePosition(callback) {
        if (this._button && this._button.measure) {
            this._button.measure((fx, fy, width, height, px, py) => {
                this._buttonFrame = {x: 0, y: py + 13, w: width, h: height};
                callback && callback();
            });
        }
    }

    show() {
        this._updatePosition(() => {
            this.setState({
                showDropdown: true
            });
        });
    }

    hide() {
        this.setState({
            showDropdown: false
        });
    }

    _renderButton() {
        return (
            <TouchableOpacity ref={button => this._button = button}
                              disabled={this.props.disabled}
                              accessible={this.props.accessible}
                              onPress={this._onButtonPress.bind(this)}>
                {
                    this.props.children ||
                    (
                        <View style={styles.button}>
                            <Text
                                style={[styles.buttonText, this.props.textStyle, GeneralStyles.fontLatoRegular, styles.orderBy, styles.orderDate]}
                                numberOfLines={1}>
                                {this.state.buttonText.toUpperCase()}
                            </Text>
                        </View>
                    )
                }
            </TouchableOpacity>
        );
    }

    _onButtonPress() {
        if (!this.props.onDropdownWillShow ||
            this.props.onDropdownWillShow() !== false) {
            this.show();
        }
    }

    openModal() {
        this._onButtonPress()
    }

    closeModal() {
        this._onRequestClose()
    }

    _renderModal() {
        if (this.state.showDropdown && this._buttonFrame) {
            let frameStyle = this._calcPosition();
            let animationType = this.props.animated ? 'fade' : 'none';
            return (
                <Modal animationType={animationType}
                       transparent={true}
                       onRequestClose={this._onRequestClose.bind(this)}
                       supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}>
                    <TouchableWithoutFeedback accessible={this.props.accessible}
                                              onPress={this._onModalPress.bind(this)}>
                        <View style={styles.modal}>
                            <ScrollView style={[styles.dropdown, this.props.dropdownStyle, frameStyle]}>
                                {this._renderDropdown()}
                            </ScrollView>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            );
        }
    }

    _calcPosition() {
        let dimensions = Dimensions.get('window');
        let windowWidth = dimensions.width;
        let windowHeight = dimensions.height;

        let dropdownHeight = (this.props.dropdownStyle && StyleSheet.flatten(this.props.dropdownStyle).height) ||
            StyleSheet.flatten(styles.dropdown).height;
        let bottomSpace = windowHeight - this._buttonFrame.y - this._buttonFrame.h;

        let rightSpace = windowWidth - this._buttonFrame.x;
        let showInBottom = bottomSpace >= dropdownHeight || bottomSpace >= this._buttonFrame.y;
        let showInLeft = rightSpace >= this._buttonFrame.x;

        let numOptions = this.props.options.length > 4 ? 3 : this.props.options.length

        let style = {
            height: numOptions * optionHeight,
            top: this._buttonFrame.y + this._buttonFrame.h,
            borderWidth: 0,
            borderColor: COLORS.PURPLE_COLOR
        };

        if (showInLeft) {
            style.left = this._buttonFrame.x;
        } else {
            let dropdownWidth = (this.props.dropdownStyle && StyleSheet.flatten(this.props.dropdownStyle).width) ||
                (this.props.style && StyleSheet.flatten(this.props.style).width) || -1;
            if (dropdownWidth !== -1) {
                style.width = dropdownWidth;
            }
            style.right = rightSpace - this._buttonFrame.w;
        }

        if (this.props.adjustFrame) {
            style = this.props.adjustFrame(style) || style;
        }

        return style;
    }

    _onRequestClose() {
        if (!this.props.onDropdownWillHide ||
            this.props.onDropdownWillHide() !== false) {
            this.hide();
        }
    }

    _onModalPress() {
        if (!this.props.onDropdownWillHide ||
            this.props.onDropdownWillHide() !== false) {
            this.hide();
        }
    }

    onChange(element) {
        this.props.onChange(element);
        this._onModalPress();
    }

    _renderDropdown() {

        let modalContent = this.props.options.map(i => {
            if (i.selected) {
                return <TouchableOpacity key={i.id} style={styles.itemContainer} onPress={() => this.onChange(i)}>
                    <Image style={styles.image} source={require('../../../../assets/images/success.png')}/>
                    <Text
                        style={[styles.item, styles.selectedItem, GeneralStyles.fontRalewaySemibold]}>{i.value.toUpperCase()}</Text>
                </TouchableOpacity>
            } else {
                return <TouchableOpacity key={i.id} style={[styles.itemContainer]}
                                         onPress={() => this.onChange(i)}>
                    <Text
                        style={[styles.item, styles.unselectedItem, GeneralStyles.fontRalewaySemibold]}>
                        {i.value.toUpperCase()}
                    </Text>
                </TouchableOpacity>
            }
        });

        return <View style={[styles.dropdownContainer]}>
            {modalContent}
        </View>
    }

}

DropdownContent.propTypes = {
    disabled: PropTypes.bool,
    defaultValue: PropTypes.string,
    options: PropTypes.array,

    accessible: PropTypes.bool,
    animated: PropTypes.bool,
    showsVerticalScrollIndicator: PropTypes.bool,

    style: PropTypes.oneOfType([PropTypes.number, PropTypes.object, PropTypes.array]),
    textStyle: PropTypes.oneOfType([PropTypes.number, PropTypes.object, PropTypes.array]),
    dropdownStyle: PropTypes.oneOfType([PropTypes.number, PropTypes.object, PropTypes.array]),

    adjustFrame: PropTypes.func,
    renderRow: PropTypes.func,
    renderSeparator: PropTypes.func,

    onDropdownWillShow: PropTypes.func,
    onDropdownWillHide: PropTypes.func,
};


DropdownContent.defaultProps = {
    options: [],
    heightRatio: 3
};

const styles = StyleSheet.create({
    dropdownContainer: {
        borderWidth: 1,
        borderColor: COLORS.BORDER_COLOR,
        borderRadius: 4
    },
    button: {},
    buttonText: {
        fontSize: 16,
    },
    modal: {
        flexGrow: 1,
        width: Dimensions.get('window').width,
        top: 5,
        left: 12,

    },
    dropdown: {
        position: 'absolute',
        backgroundColor: 'white',
        width: Dimensions.get('window').width - 24,
        shadowOpacity: 0.2,
        shadowRadius: 1,
        shadowOffset: {height: 1, width: 0},
    },
    loading: {
        alignSelf: 'center'
    },
    rowText: {
        paddingHorizontal: 6,
        paddingVertical: 10,
        fontSize: 11,
        color: 'gray',
        backgroundColor: 'white',
        textAlignVertical: 'center',
    },
    highlightedRowText: {
        color: 'black'
    },
    orderBy: {
        lineHeight: 16,
        letterSpacing: 1.5,
        color: '#72727c'
    },
    orderDate: {
        color: '#333337'
    },
    itemContainer: {
        flexDirection: 'row',
        paddingTop: 12,
        paddingBottom: 12
    },
    item: {
        fontSize: 16,
        lineHeight: 16,
        letterSpacing: 1.5,
        color: '#b3b3bb',
    },
    selectedItem: {
        color: '#333337'
    },
    unselectedItem: {
        marginLeft: 28
    },
    image: {
        marginLeft: 8,
        marginRight: 8,
        marginTop: 1,
        width: 12,
        height: 12,
    }
});

export default DropdownContent