import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Text, StyleSheet, Image, TouchableOpacity} from 'react-native'
import GeneralStyles from '../../../assets/styles/GeneralStyles'

class NavigatorBarButton extends Component {

    render() {
        const text = <Text
            style={[styles.text, GeneralStyles.fontRalewaySemibold]}> {this.props.title.toUpperCase()} </Text>;
        const images = <Image source={this.props.icon} style={styles.icon}/>;
        return (
            <TouchableOpacity style={[styles.container, this.props.style]} onPress={this.props.onPress}>
                {this.props.icon ? images : text}
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderWidth: 0,
        justifyContent: "center"
    },
    text: {
        color: "#72727c",
        lineHeight: 12,
        letterSpacing: 2,
        fontSize: 12,
        marginRight: 15,
    },
    icon: {
        marginLeft: 15,
        marginRight: 15,
        width: 32,
        height: 32,
    }
});

NavigatorBarButton.propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func,
    icon: PropTypes.number,
};

NavigatorBarButton.defaultProps = {
    title: "",
};

export default NavigatorBarButton;