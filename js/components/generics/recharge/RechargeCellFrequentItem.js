import React, {Component} from 'react';
import {
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import {GLOBAL, LOGIN} from "../../../constants/LabelConstants";
import TextBoxType from "../TextBoxType";
import TextBoxPhone from '../TextBoxPhone';
import TextBox from '../TextBox';
import Dropdown from '../dropdown/Dropdown';
import Button from "../Button";
import * as ButtonType from "../ButtonType";
import * as ValidateConstant from "../../../constants/ValidateConstant";
import {FONTS, COLORS, ICON} from '../../../constants/StylesConstants'
import PropTypes from 'prop-types';

class RechargeCellFrequentItem extends Component {

    constructor(props) {
        super(props);
        let amountSelected = {
            id: 20,
            value: "20 + 30 CUC",
            selected: true
        };
        this.state = {
            email: this.props.email,
            password: null,
            emailError: ValidateConstant.ERROR_REQUIRED,
            showErrors: false,
            amountSelected: amountSelected,
            amounts: [
                {
                    id: 10,
                    value: "10 CUC",
                    selected: false
                },
                amountSelected,
                {
                    id: 50,
                    value: "50 CUC",
                    selected: false
                },
                {
                    id: 100,
                    value: "100 CUC",
                    selected: false
                }
            ]
        };
        this.onChangeAmount = this.onChangeAmount.bind(this);
    }

    onChangeAmount(item, error) {
        let amountSelected = this.state.amountSelected;
        let amounts = this.state.amounts.map(value => {
            if (value.id === item.id) {
                amountSelected = {
                    id: value.id,
                    value: value.value,
                    selected: true
                };
                return amountSelected;
            } else {
                return {
                    id: value.id,
                    value: value.value,
                    selected: false
                };
            }
        });
        this.setState({amounts: amounts, amountSelected: amountSelected})
    }

    render() {

        let button = null;
        if (this.props.showRechargeButton) {
            button = <Button value={"Recargar"} type={ButtonType.GREEN}
                             containerButtonStyle={styles.rechargeButton} diffWidthAnimated={-3}
                             onPress={this.props.onPressRecharge}/>
        }
        let removeButton = null;
        if (this.props.showRemoveButton) {
            removeButton = <TouchableOpacity style={styles.containerRemove}>
                <Image style={styles.imageRemove} source={require('../../../../assets/images/close.png')}/>
            </TouchableOpacity>
        }

        return (

            <View style={styles.container}>
                <View style={styles.containerFlag}>
                    <Image style={styles.imageFlag} source={require('../../../../assets/images/flag-cuba.png')}/>

                    <View style={styles.containerContact}>
                        <Text style={styles.contactName}>{"My Contact 1".toUpperCase()}</Text>
                        <Text style={styles.contactPhone}>+53 54152844</Text>
                    </View>

                    {removeButton}

                </View>
                <Dropdown style={styles.inputMarginBottom} heightRatio={4}
                          text={this.state.amountSelected.value}
                          options={this.state.amounts}
                          onChange={this.onChangeAmount}
                          required={true}
                          disabled={this.props.disabledDropdown}
                          hasError={this.state.showErrors}
                          labelError=""
                          label={"Monto a recargar"}/>

                {button}
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 7,
        borderWidth: 1,
        borderColor: COLORS.BORDER_COLOR,
        borderRadius: 4,
        marginTop: 20,
        paddingRight: 12,
        paddingLeft: 12
    },
    containerFlag: {
        flexDirection: 'row',
    },
    containerContact: {
        flexDirection: 'column',
        marginLeft: 10,
    },
    imageFlag: {},
    contactName: {
        fontSize: 16,
        color: COLORS.BLUE_COLOR_DARK,
        fontWeight: 'bold',
    },
    contactPhone: {
        fontSize: 14,
        color: COLORS.ALERT_COLOR,
        marginTop: 3
    },
    inputMarginBottom: {
        marginTop: 10
    },
    rechargeButton: {
        marginTop: 10,
        paddingRight: 12,
        paddingLeft: 12,
    },
    containerRemove: {
        backgroundColor: COLORS.ALERT_COLOR,
        padding: 7,
        borderRadius: 30,
        height: 30,
        width: 30,
        position: 'absolute',
        right: 0,
    },
    imageRemove: {
        height: 15,
        width: 15,
    }
});


RechargeCellFrequentItem.propTypes = {
    showRechargeButton: PropTypes.bool,
    disabledDropdown: PropTypes.bool,
    showRemoveButton: PropTypes.bool,
    onPressRecharge: PropTypes.func,
};

RechargeCellFrequentItem.defaultProps = {
    showRechargeButton: true,
    showRemoveButton: true,
    disabledDropdown: false
};

export default RechargeCellFrequentItem;