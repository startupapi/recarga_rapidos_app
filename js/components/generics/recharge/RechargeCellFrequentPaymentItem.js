import React, {Component} from 'react';
import {
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import {GLOBAL, LOGIN} from "../../../constants/LabelConstants";
import TextBoxType from "../TextBoxType";
import TextBoxPhone from '../TextBoxPhone';
import TextBox from '../TextBox';
import Dropdown from '../dropdown/Dropdown';
import Button from "../Button";
import * as ButtonType from "../ButtonType";
import * as ValidateConstant from "../../../constants/ValidateConstant";
import {FONTS, COLORS, ICON} from '../../../constants/StylesConstants'
import PropTypes from 'prop-types';

class RechargeCellFrequentPaymentItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        let removeButton = null;
        if (this.props.showRemoveButton) {
            removeButton = <TouchableOpacity style={styles.containerRemove}>
                <Image style={styles.imageRemove} source={require('../../../../assets/images/close.png')}/>
            </TouchableOpacity>
        }

        return (

            <View style={styles.container}>
                <View style={styles.containerFlag}>
                    <Image style={styles.imageFlag} source={require('../../../../assets/images/flag-cuba.png')}/>

                    <View style={styles.containerContact}>
                        <Text style={styles.contactName}>{"My Contact 1".toUpperCase()}</Text>
                        <Text style={styles.contactPhone}>+53 54152844</Text>
                        <Text style={styles.contactReceive}>Recibe en Cuba <Text
                            style={[styles.contactReceive, styles.contactReceiveValue]}>50 CUC</Text></Text>
                    </View>
                    {removeButton}
                </View>
                <View style={styles.containerValue}>
                    <Text style={styles.inputValue}>{this.props.text.toUpperCase()}</Text>
                </View>

            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 7,
        borderWidth: 1,
        borderColor: COLORS.BORDER_COLOR,
        borderRadius: 4,
        marginTop: 20,
        paddingRight: 12,
        paddingLeft: 12
    },
    containerFlag: {
        flexDirection: 'row',
    },
    containerContact: {
        flexDirection: 'column',
        marginLeft: 10,
    },
    imageFlag: {},
    contactName: {
        fontSize: 16,
        color: COLORS.BLUE_COLOR_DARK,
        fontWeight: 'bold',
    },
    contactPhone: {
        fontSize: 14,
        color: COLORS.BLUE_COLOR_DARK,
        marginTop: 3
    },
    contactReceive: {
        fontSize: 15,
        color: COLORS.ALERT_COLOR,
        marginTop: 3
    },
    inputMarginBottom: {
        marginTop: 10
    },
    rechargeButton: {
        marginTop: 10,
        paddingRight: 12,
        paddingLeft: 12,
    },
    containerRemove: {
        backgroundColor: COLORS.ALERT_COLOR,
        padding: 7,
        borderRadius: 30,
        height: 30,
        width: 30,
        position: 'absolute',
        right: 0,
    },
    imageRemove: {
        height: 15,
        width: 15,
    },
    inputValue: {
        fontSize: 18,
        lineHeight: 18,
        letterSpacing: 1.5,
        fontWeight: 'bold',
        color: COLORS.BLUE_COLOR_DARK,
    },
    contactReceiveValue: {
        fontWeight: 'bold'
    },
    containerValue: {
        marginTop: 10,
        height: 45,
        borderColor: COLORS.BORDER_COLOR,
        borderWidth: 2,
        borderRadius: 5,
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 13
    }
});


RechargeCellFrequentPaymentItem.propTypes = {
    showRemoveButton: PropTypes.bool,
};

RechargeCellFrequentPaymentItem.defaultProps = {
    showRemoveButton: true,
};

export default RechargeCellFrequentPaymentItem;