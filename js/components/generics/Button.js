import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, Text, StyleSheet, Image, TouchableOpacity, Animated, Dimensions} from 'react-native'
import {COLORS, FONTS} from '../../constants/StylesConstants'
import * as ButtonType from '../generics/ButtonType'

class Button extends Component {
    constructor(props) {
        super(props)
        this.state = {
            width: new Animated.Value(10),
            textWidth: new Animated.Value(0),
            opacityText: new Animated.Value(0),
            buttonStyle: [],
            textStyle: [],
        }
        this.setStyles = this.setStyles.bind(this)
    }

    componentDidMount() {
        this.setStyles(this.props.type)
        let modalWidth = this.props.diffWidthAnimated
        Animated.parallel([
            Animated.spring(this.state.width, {
                toValue: Dimensions.get('window').width - 56 - modalWidth,
                duration: 600,
                tension: -4, // Slow
                friction: 4,  // Oscillate a lot
            }),
            Animated.timing(this.state.textWidth, {
                toValue: Dimensions.get('window').width - 70 - modalWidth,
                duration: 700,
            }),
            Animated.timing(this.state.opacityText, {
                toValue: 1,
                duration: 600,
                delay: 50
            })
        ]).start()
    }

    setStyles(type) {
        switch (type) {
            case ButtonType.WHITE:
                this.setState({
                    buttonStyle: [{
                        backgroundColor: COLORS.WHITE, 
                        borderColor: COLORS.BLUE_COLOR, 
                        borderWidth: 1
                    }],
                    textStyle:[{
                        color: COLORS.BLUE_COLOR 
                    }]
                });
                return;
            case ButtonType.BLUE:
                this.setState({
                    buttonStyle: [{
                        backgroundColor: COLORS.BLUE_COLOR,
                        borderColor: COLORS.BLUE_COLOR,
                        borderWidth: 1
                    }],
                    textStyle:[{
                        color: COLORS.WHITE
                    }]
                });
                return;
            case ButtonType.GREEN:
                this.setState({
                    buttonStyle: [{
                        backgroundColor: COLORS.GREEN_COLOR,
                        borderColor: COLORS.GREEN_COLOR,
                        borderWidth: 1
                    }],
                    textStyle:[{
                        color: COLORS.WHITE
                    }]
                });
                return;
            case ButtonType.PURPLE:
                this.setState({
                    buttonStyle: [{
                        backgroundColor: COLORS.PURPLE_COLOR,
                        borderColor: COLORS.WHITE,
                        borderWidth: 1
                    }],
                    textStyle:[{
                        color: COLORS.WHITE
                    }]
                });
                return;
            case ButtonType.PURPLE_LIGHT:
                this.setState({
                    buttonStyle: [{
                        backgroundColor: COLORS.PURPLE_LIGHT_COLOR,
                        borderColor: COLORS.WHITE,
                        borderWidth: 1
                    }],
                    textStyle:[{
                        color: COLORS.WHITE
                    }]
                });
                return;
            case ButtonType.GRAY:
                this.setState({
                    buttonStyle: [{
                        backgroundColor: COLORS.SWIPE_GRAY, 
                        borderColor: COLORS.SWIPE_GRAY, 
                        borderWidth: 1
                    }],
                    textStyle:[{
                         color: COLORS.WHITE 
                    }]
                });
                return;
            case ButtonType.FLAT:
                this.setState({
                    buttonStyle: [{
                        borderWidth: 1,
                        borderColor: COLORS.BLUE_COLOR_DARK,
                    }],
                    textStyle:[{
                         color: COLORS.BLUE_COLOR_DARK
                    }]
                });
                return;
        }
    }

    render() {
        let btn = <View style={[styles.container, this.state.buttonStyle, this.props.buttonStyle]}>
            <Text
                style={[styles.value, , this.state.textStyle, this.state.textStyle, FONTS.fontRalewaySemibold]}>{this.props.value.toUpperCase()}</Text>
        </View>
        if (this.props.animate) {
            btn = <Animated.View style={[styles.container, this.state.buttonStyle, this.props.buttonStyle, {width: this.state.width}]}>
                <Animated.Text ellipsizeMode="middle" numberOfLines={1}
                               style={[styles.value, FONTS.fontRalewaySemibold, this.state.textStyle, this.props.textStyle, {
                                   opacity: this.state.opacityText,
                                   width: this.state.textWidth
                               }]}>{this.props.value.toUpperCase()}</Animated.Text>
            </Animated.View>
        }
        return (
            <TouchableOpacity style={[this.props.containerButtonStyle, this.props.animate ? styles.itemsAnimate : null]}
                              onPress={this.props.onPress} disabled={this.props.disabled}>
                {btn}
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 15,
        paddingBottom: 15,
        flexDirection: 'column',
        alignItems: 'center',
        borderRadius: 50,
    },
    value: {
        fontSize: 15,
        letterSpacing: 2,
        lineHeight: 19,
        textAlign: 'center'
    },
    itemsAnimate: {
        justifyContent: 'center',
        alignItems: 'center',
    }
});


Button.propTypes = {
    value: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    animate: PropTypes.bool,
    onModal: PropTypes.bool,
    diffWidthAnimated: PropTypes.number,
    buttonStyle: PropTypes.number,
    textStyle: PropTypes.number,
    containerButtonStyle: PropTypes.number,
    type: PropTypes.string
};

Button.defaultProps = {
    animate: true,
    onModal: false,
    diffWidthAnimated: 0,
    value: "",
    title: "",
    type: ButtonType.WHITE
};

export default Button