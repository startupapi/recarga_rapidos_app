import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity, Platform, Image} from 'react-native';
import PropTypes from 'prop-types';
import TextBoxType from "./TextBoxType"
import {isValidEmail, isEmpty, isValidNumber, isValidIdentification} from "../../helpers/ValidationUtils"
import * as ValidateConstant from "../../constants/ValidateConstant"
import {COLORS, FONTS, ICON} from '../../constants/StylesConstants'

class TextBoxPhone extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showPass: true,
            text: this.props.value,
            remainingCharacters: this.props.maxLength,
            showRemainingCharacters: this.props.showRemainingCharacters && this.props.maxLength ? true : false
        };
        this.onChangeText = this.onChangeText.bind(this);
        this.onPressShowPassword = this.onPressShowPassword.bind(this);
    }

    onPressShowPassword() {
        this.setState({showPass: !this.state.showPass})
    }

    onChangeText(text) {

        this.setState({text, remainingCharacters: this.props.maxLength - text.length});
        if (this.props.required && isEmpty(text)) {
            this.props.onChangeText(text, ValidateConstant.ERROR_REQUIRED);
        } else {
            switch (this.props.type) {
                case TextBoxType.EMAIL:
                    isValidEmail(text) ? this.props.onChangeText(text, ValidateConstant.ERROR_EMAIL_FORMAT) : this.props.onChangeText(text, null);
                    break;
                case TextBoxType.DOC_NUMBER:
                    isValidNumber(text) ? this.props.onChangeText(text, ValidateConstant.ERROR_NUMBER_FORMAT) : isValidIdentification(text) ? this.props.onChangeText(text, ValidateConstant.ERROR_ID_FORMAT) : this.props.onChangeText(text, null);
                    break;
                case TextBoxType.NUMBER:
                    isValidNumber(text) ? this.props.onChangeText(text, ValidateConstant.ERROR_NUMBER_FORMAT) : this.props.onChangeText(text, null);
                    break;
                default:
                    this.props.onChangeText(text, null);
            }
        }
    }

    render() {

        let labelShow = null;
        let iconError = null;
        let labelError = null;

        let labelRequiredAst = this.props.required ? <Text style={styles.labelRequiredAst}>*</Text> : null;

        const labelText = <Text
            style={[styles.label, FONTS.fontRalewaySemibold, this.props.hasError && this.props.labelError !== null ? styles.labelInputError : null]}>{this.props.label.toUpperCase()} {labelRequiredAst} </Text>;


        if (this.props.hasError) {
            labelError = this.props.labelError !== null ? <Text
                style={[styles.labelError, FONTS.fontRalewayBold]}>{this.props.labelError.toUpperCase()}</Text> : null;
        }

        if (this.props.type === TextBoxType.PASSWORD) {
            labelShow = <TouchableOpacity onPress={this.onPressShowPassword}>
                <Text style={[styles.label, styles.labelShow, FONTS.fontRalewaySemibold]}>MOSTRAR</Text>
            </TouchableOpacity>
        }

        if (this.props.hasError && this.props.type !== TextBoxType.PASSWORD && this.props.labelError !== null && !this.props.hideWarningIcon) {
            iconError = <Image source={ICON.ALERT_LABEL}/>;
        }

        let labelTextContainer = <View style={styles.labelTextContainer}>
            {labelText}
            {iconError}
            {labelShow}
        </View>;

        let keyboardType = 'default'
        switch (this.props.type) {
            case TextBoxType.EMAIL:
                keyboardType = 'email-address'
                break;
            case TextBoxType.NUMBER:
                keyboardType = 'numeric'
                break;
        }


        let textInput = this.props.disabled ? <Text style={[styles.textInput, FONTS.fontLatoRegular]}
                                                    onPress={this.props.onPress}>{this.state.text}</Text>
            : <TextInput
                style={[styles.textInput, FONTS.fontLatoRegular, this.props.inputStyle]}
                multiline={this.props.multiline}
                maxLength={this.props.maxLength}
                numberOfLines={this.props.numberOfLines}
                onChangeText={this.onChangeText}
                value={this.state.text.toString()}
                underlineColorAndroid='transparent'
                autoCorrect={false}
                secureTextEntry={this.props.type === TextBoxType.PASSWORD && this.state.showPass}
                placeholder={this.props.placeholder}
                autoFocus={true}
                keyboardType={keyboardType}
                onBlur={this.props.onBlur}
                onFocus={this.props.onFocus}

            />;
        let remainingCharacters = this.state.showRemainingCharacters ? <Text
            style={[FONTS.fontLatoRegular, styles.remainingCharacters]}>{this.state.remainingCharacters} caracteres
            restantes</Text> : null;

        return (
            <View style={[styles.container, this.props.style]}>
                <View
                    style={[styles.textBoxContainer, this.props.hasError && this.props.labelError !== null ? styles.textBoxContainerWithError : this.props.type === "borderLess" ? styles.textBoxContainerNoBorder : null]}>

                    {labelTextContainer}

                    <View style={[styles.containerFlag]}>
                        <Image style={[styles.flag]} source={require("../../../assets/images/flag-cuba-circle.png")}/>
                        <Text style={[styles.textCountryCode]}>+53 </Text>
                        {textInput}
                    </View>
                </View>
                {remainingCharacters}
                {labelError}
                <Text style={[styles.textCountryCodeExample]}>Ejemplo: +53 53999999</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    textBoxContainer: {
        height: 65,
        borderColor: COLORS.BORDER_COLOR,
        borderWidth: 2,
        borderRadius: 5,
        paddingLeft: 5,
        paddingRight: 5,
    },
    textBoxContainerWithError: {
        borderColor: COLORS.ALERT_BORDER,
    },
    textBoxContainerNoBorder: {
        borderColor: COLORS.WHITE,
    },
    labelTextContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    label: {
        color: COLORS.TEXT_LIGHTEEN_3,
        fontSize: 12,
        letterSpacing: 2,
        alignSelf: 'flex-start',
        marginLeft: 5,
        marginTop: 5
    },
    textInput: {
        height: 30,
        flex: 1,
        fontSize: 20,
        lineHeight: 30,
        color: COLORS.TEXT_COLOR,
        marginTop: 4,
        ...Platform.select({
            android: {height: 44}
        }),
    },
    labelInputError: {
        color: COLORS.ALERT_COLOR,
    },
    labelError: {
        color: COLORS.ALERT_COLOR,
        letterSpacing: 1.5,
        fontSize: 10,
        marginTop: 4,
        textAlign: "left",
        marginRight: -2
    },
    labelShow: {
        alignSelf: 'flex-end',
        marginRight: 5
    },
    remainingCharacters: {
        alignSelf: 'flex-end',
        color: COLORS.TEXT_LIGHTEEN_3,
        fontSize: 12,
        marginTop: 4,
        lineHeight: 16
    },
    labelRequiredAst: {
        color: COLORS.ALERT_COLOR
    },
    flag: {
        width: 20,
        height: 20,
        marginTop: 9
    },
    containerFlag: {
        flexDirection: 'row'
    },
    textCountryCode: {
        marginTop: 8,
        fontSize: 18,
        marginLeft: 2,
        marginRight: 2,
        letterSpacing: 1.5,
        color: '#333337'
    },
    textCountryCodeExample: {
        fontSize: 12,
        color: COLORS.TEXT_LIGHTEEN_3,
        marginTop: 5
    }
});

TextBoxPhone.propTypes = {
    hasError: PropTypes.bool,
    labelError: PropTypes.string,
    onChangeText: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    required: PropTypes.bool
};

TextBoxPhone.defaultProps = {
    value: "",
    label: "",
    labelError: "",
    required: false
}

export default TextBoxPhone