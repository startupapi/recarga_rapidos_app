import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    Text,
    Animated
} from 'react-native'
import GeneralStyles from '../../../assets/styles/GeneralStyles'
import {FONTS, COLORS, ICON} from '../../constants/StylesConstants'

const SPRING_CONFIG = {tension: 20}
class NavBarTitle extends Component {

    constructor(props) {
        super(props)
        this.state = {
            titleOpacity : new Animated.Value(0),
            titleFontSize : new Animated.Value(1)
        }
    }

    componentDidMount() {  
    }

    render() {
        if (this.props.showNavTitle) {
            Animated.parallel([
                Animated.timing(
                this.state.titleFontSize,
                {
                    toValue: 13,
                    duration: 300
                }
            ),
            Animated.timing(
                this.state.titleOpacity,
                {
                    toValue: 1,
                    duration: 300
                }
            )
            ]).start();
           
        } else {
            Animated.parallel([
                Animated.timing(
                this.state.titleFontSize,
                {
                    toValue: 1,
                    duration: 300
                }
                ),
                Animated.timing(
                    this.state.titleOpacity,
                    {
                        toValue: 0,
                        duration: 300
                    }
                )
            ]).start();
            
        }
        return (
            <Animated.View style={[{opacity: this.state.titleOpacity, paddingBottom: 3}]}>
                <Animated.Text style={[styles.container, GeneralStyles.fontRalewayBold, {fontSize: this.state.titleFontSize}]}>{this.props.title.toUpperCase()}</Animated.Text>
            </Animated.View>
            )
    }
}

const styles = StyleSheet.create({
    container: {
        fontSize: 13,
        lineHeight: 16,
        letterSpacing: 1.5,
        color: COLORS.BLUE_COLOR_DARK,
        fontWeight: 'bold'
    },
});

export default NavBarTitle