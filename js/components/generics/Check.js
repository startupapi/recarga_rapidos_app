import React, {Component} from 'react';
import {StyleSheet, Text, View, CheckBox, Image, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import TextBoxType from "./TextBoxType"
import * as ValidateConstant from "../../constants/ValidateConstant"
import {COLORS, FONTS, ICON} from '../../constants/StylesConstants'

class Check extends Component {

    constructor(props) {
        super(props);

        this.renderUnchecked = this.renderUnchecked.bind(this)
        this.renderChecked = this.renderChecked.bind(this)
        this.renderTextChecked = this.renderTextChecked.bind(this)
        this.renderTextUnchecked = this.renderTextUnchecked.bind(this)
        this.onPress = this.onPress.bind(this)
    }

    renderUnchecked() {
        return <TouchableOpacity style={styles.checkbox} onPress={this.onPress}>
        </TouchableOpacity>
    }

    renderChecked() {
        return <TouchableOpacity style={[styles.checkbox, styles.checked]} onPress={this.onPress}>
            <Image style={[styles.checkedImage]} source={ICON.SUCCESS_CHECK}/>
        </TouchableOpacity>
    }

    renderTextChecked() {
        return this.renderTextUnchecked()
    }

    renderTextUnchecked() {
        return <View style={styles.textContainer}>
            <Text style={[styles.text, FONTS.fontLatoRegular]} onPress={this.onPress}>{this.props.text}</Text>
        </View>
    }

    onPress() {
        this.props.onChangeChecked(!this.props.checked)
    }

    render() {
        let labelError = null;

        if (this.props.hasError) {
            labelError = this.props.labelError !== null ? <Text
                style={[styles.labelError, FONTS.fontRalewayBold]}>{this.props.labelError.toUpperCase()}</Text> : null;
        }

        let checkbox = this.props.checked ? this.renderChecked() : this.renderUnchecked();
        let text = this.props.checked ? this.renderTextChecked() : this.renderTextUnchecked();
        let component = null;
        if(this.props.visible){
            component = <View style={[this.props.style]}>
                    <TouchableOpacity style={styles.checkContainer}>
                        {checkbox}
                        {text}
                    </TouchableOpacity>
                    {labelError}
                </View>
        }

        return component;
    }
}

const styles = StyleSheet.create({
    container: {},
    checkContainer: {
        flexDirection: 'row'
    },
    checkbox: {
        borderWidth: 2,
        borderColor: COLORS.BLUE_COLOR_DARK,
        width: 32,
        height: 32,
        borderRadius: 6,
        marginTop: 5
    },
    checked: {
        backgroundColor: COLORS.BLUE_COLOR_DARK,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textContainer: {
        paddingLeft: 16,
        flex: 1,
        marginTop: 7,
    },
    text: {
        fontSize: 16,
        lineHeight: 24,
        color: COLORS.TEXT_LIGHTEEN_4
    },
    labelError: {
        color: COLORS.ALERT_COLOR,
        letterSpacing: 1.5,
        fontSize: 10,
        marginTop: 4,
        textAlign: "right",
        marginRight: -2
    },
    checkedImage:{
        width: 28,
        height: 28,
    }
});

CheckBox.propTypes = {
    checked: PropTypes.bool,
    visible: PropTypes.bool,
    onChangeChecked: PropTypes.func,
};

CheckBox.defaultProps = {
    checked: true,
    visible: true
};

export default Check