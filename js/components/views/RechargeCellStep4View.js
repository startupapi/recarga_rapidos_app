import React, {Component} from 'react';
import {View, Text, StyleSheet, ScrollView, Image} from 'react-native';
import {GLOBAL, LOGIN} from "../../constants/LabelConstants";
import Button from "../generics/Button";
import * as ButtonType from "../generics/ButtonType";
import {COLORS, ICON} from '../../constants/StylesConstants'
import NavigationBar from '../generics/navbar/NavigationBar'
import NavBarTitle from '../generics/NavBarTitle'
import NavigatorBarButton from "../generics/NavigatorBarButton"
import RechargeCellFrequentPaymentItem from "../generics/recharge/RechargeCellFrequentPaymentItem"
import PropTypes from 'prop-types';
import StepIndicator from 'react-native-step-indicator';
import {LABELS, CUSTOM_STYLES} from '../../constants/WizardConstants'
import TextBoxType from "../generics/TextBoxType";
import TextBox from '../generics/TextBox';
import Radio from '../generics/Radio'
import Dropdown from '../generics/dropdown/Dropdown'
import Check from '../generics/Check'
import * as ValidateConstant from "../../constants/ValidateConstant";

class RechargeCellStep4View extends Component {

    constructor(props) {
        super(props);
        let amountSelected = {
            id: 20,
            value: "Visa termina 8900",
            selected: true
        };
        let expirationDateSelected = {
            id: 4,
            value: "4",
            selected: true
        };
        this.state = {
            termsRegisterMethod: true,
            termsNewMethod: false,
            amountSelected: amountSelected,
            expirationDateSelected: expirationDateSelected,
            amounts: [
                amountSelected,
                {
                    id: 50,
                    value: "Mastercard termina 6785",
                    selected: false
                }
            ],
            expirationDates: [
                {
                    id: 1,
                    value: "1",
                    selected: false
                },
                expirationDateSelected,
                {
                    id: 2,
                    value: "2",
                    selected: false
                },
                {
                    id: 3,
                    value: "3",
                    selected: false
                }
            ]
        };
        this.onChangeCheckedRegisterMethod = this.onChangeCheckedRegisterMethod.bind(this);
        this.onChangeCheckedNewMethod = this.onChangeCheckedNewMethod.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
    }

    onChangeAmount(item, error) {
        let amountSelected = this.state.amountSelected;
        let amounts = this.state.amounts.map(value => {
            if (value.id === item.id) {
                amountSelected = {
                    id: value.id,
                    value: value.value,
                    selected: true
                };
                return amountSelected;
            } else {
                return {
                    id: value.id,
                    value: value.value,
                    selected: false
                };
            }
        });
        this.setState({amounts: amounts, amountSelected: amountSelected})
    }

    onChangeCheckedRegisterMethod(checked) {
        this.setState({termsRegisterMethod: true, termsNewMethod: false})
    }

    onChangeCheckedNewMethod(checked) {
        this.setState({termsRegisterMethod: false, termsNewMethod: true})
    }

    render() {
        return (
            <View style={styles.containerMain}>

                <NavigationBar
                    containerStyle={styles.navBarWhite}
                    title={<NavBarTitle title={"Elejir método de pago"} showNavTitle={true} isAnimated={false}/>}
                />

                <ScrollView>
                    <View style={styles.stepIndicator}>
                        <StepIndicator
                            stepCount={4}
                            customStyles={CUSTOM_STYLES}
                            currentPosition={3}
                            labels={LABELS}
                        />
                    </View>

                    <View style={styles.containerBorderBlue}>
                        <Image style={styles.loadingBlue} source={require('../../../assets/images/loadingBlue.gif')}/>
                        <Text style={styles.loadingTextBlue}>Procesando su transacción</Text>
                    </View>

                    <View style={styles.containerBorderGreen}>
                        <Text style={styles.loadingTextGreen}>Su transacción ha sido procesanda correctamente</Text>
                    </View>

                    <View style={styles.containerBorderRed}>
                        <Text style={styles.loadingTextRed}>Su transacción no ha sido procesanda correctamente, estas
                            pueden ser algunas causas</Text>

                        <View style={[styles.containerCausesItem, styles.containerCausesItemFirstMargin]}>
                            <View style={styles.containerNumberCauses}>
                                <Text style={styles.numberCauses}>1</Text>
                            </View>
                            <Text style={styles.textCauses}>Su banco no ha autorizado el pago</Text>
                        </View>

                        <View style={[styles.containerCausesItem,styles.containerCausesItemOtherMargin]}>
                            <View style={styles.containerNumberCauses}>
                                <Text style={styles.numberCauses}>2</Text>
                            </View>
                            <Text style={styles.textCauses}>Su tarjeta de crédito no es válida</Text>
                        </View>

                        <View style={[styles.containerCausesItem,styles.containerCausesItemOtherMargin]}>
                            <View style={styles.containerNumberCauses}>
                                <Text style={styles.numberCauses}>3</Text>
                            </View>
                            <Text style={styles.textCauses}>Lorem ipsum il lort</Text>
                        </View>

                    </View>

                    <Button value={"Realizar otra recarga"} type={ButtonType.GREEN}
                            containerButtonStyle={styles.finishButton} diffWidthAnimated={-30}
                            onPress={() => ""}/>


                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerMain: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        paddingLeft: 12,
        paddingRight: 12,
    },
    cancelButton: {
        marginTop: 10,
        marginBottom: 15
    },
    finishButton: {
        marginTop: 10
    },
    labelTitle: {
        fontSize: 13,
        lineHeight: 16,
        letterSpacing: 1.5,
        color: COLORS.BLUE_COLOR_DARK,
        fontWeight: 'bold'
    },
    checkStyle: {
        marginTop: 3
    },
    stepIndicator: {
        marginTop: 7
    },
    containerBorderBlue: {
        padding: 7,
        borderWidth: 1,
        borderColor: COLORS.BLUE_COLOR_DARK,
        borderRadius: 4,
        marginTop: 10,
        paddingRight: 12,
        paddingLeft: 12
    },
    containerBorderGreen: {
        padding: 7,
        borderWidth: 1,
        borderColor: COLORS.GREEN_COLOR,
        borderRadius: 4,
        marginTop: 10,
        paddingRight: 12,
        paddingLeft: 12
    },
    containerBorderRed: {
        padding: 7,
        borderWidth: 1,
        borderColor: COLORS.ALERT_BORDER,
        borderRadius: 4,
        marginTop: 10,
        paddingRight: 12,
        paddingLeft: 12
    },
    inputMarginBottom: {
        marginTop: 10
    },
    loadingBlue: {
        alignSelf: 'center',
        height: 50,
        width: 50
    },
    loadingTextBlue: {
        color: COLORS.BLUE_COLOR_DARK,
        alignSelf: 'center',
        marginTop: 10,
        fontSize: 18,
        fontWeight: '600'
    },
    loadingTextGreen: {
        color: COLORS.GREEN_COLOR,
        fontSize: 18,
        fontWeight: '600'
    },
    loadingTextRed: {
        color: COLORS.ALERT_BORDER,
        fontSize: 18,
        fontWeight: '600'
    },
    containerCausesItemFirstMargin: {
        marginTop: 15
    },
    containerCausesItemOtherMargin: {
        marginTop: 10
    },
    containerCausesItem: {
        flexDirection: 'row',
    },
    containerNumberCauses: {
        backgroundColor: COLORS.ALERT_BORDER,
        height: 24,
        width: 24,
        borderRadius: 12
    },
    numberCauses: {
        color: COLORS.WHITE,
        marginTop: 3,
        alignSelf: 'center'
    },
    textCauses: {
        color: COLORS.ALERT_BORDER,
        marginLeft: 5,
        marginTop: 3
    }

});


RechargeCellStep4View.propTypes = {
    onPressBack: PropTypes.func,
};

export default RechargeCellStep4View;