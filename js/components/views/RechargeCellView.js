import React, {Component} from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import {GLOBAL, LOGIN} from "../../constants/LabelConstants";
import TextBoxType from "../generics/TextBoxType";
import TextBoxPhone from '../generics/TextBoxPhone';
import TextBox from '../generics/TextBox';
import Dropdown from '../generics/dropdown/Dropdown';
import Button from "../generics/Button";
import * as ButtonType from "../generics/ButtonType";
import * as ValidateConstant from "../../constants/ValidateConstant";
import {FONTS, COLORS, ICON} from '../../constants/StylesConstants'
import NavigationBar from '../generics/navbar/NavigationBar'
import NavBarTitle from '../generics/NavBarTitle'
import NavigatorBarButton from "../generics/NavigatorBarButton"
import PropTypes from 'prop-types';

class RechargeCellView extends Component {

    constructor(props) {
        super(props);
        let amountSelected = {
            id: 20,
            value: "20 + 30 CUC",
            selected: true
        };
        this.state = {
            email: this.props.email,
            password: null,
            emailError: ValidateConstant.ERROR_REQUIRED,
            showErrors: false,
            amountSelected: amountSelected,
            amounts: [
                {
                    id: 10,
                    value: "10 CUC",
                    selected: false
                },
                amountSelected,
                {
                    id: 50,
                    value: "50 CUC",
                    selected: false
                },
                {
                    id: 100,
                    value: "100 CUC",
                    selected: false
                }
            ]
        };
        this.onChangeAmount = this.onChangeAmount.bind(this);

    }

    onChangeAmount(item, error) {
        console.log(".............", item, error)
        let amountSelected = this.state.amountSelected;
        let amounts = this.state.amounts.map(value => {
            console.log("......,,,,,,,,,,,,,.......", value)

            if (value.id === item.id) {
                amountSelected = {
                    id: value.id,
                    value: value.value,
                    selected: true
                };
                return amountSelected;
            } else {
                return {
                    id: value.id,
                    value: value.value,
                    selected: false
                };
            }
        });
        this.setState({amounts: amounts, amountSelected: amountSelected})
    }

    render() {


        return (
            <View style={styles.containerMain}>

                <NavigationBar
                    containerStyle={styles.navBarWhite}
                    title={<NavBarTitle title={"Recargar celular"} showNavTitle={true} isAnimated={false}/>}
                    leftButton={<NavigatorBarButton title={GLOBAL.BACK} icon={ICON.MENU}
                                                    onPress={this.props.onOpenMenu}/>}
                />

                {/*<View style={styles.labelSeparator}>*/}
                {/*<Text*/}
                {/*style={[FONTS.fontRalewayBold, styles.labelSeparatorText]}>{"Insertar Teléfono para iniciar la recarga."}</Text>*/}
                {/*</View>*/}

                <View style={styles.container}>

                    <TextBox defaultText="" label={"Nombre a recargar"}
                             style={styles.nameTextBox}
                             onChangeText={() => ""}
                             hasError={this.state.showErrors}
                             labelError={this.state.emailError}
                             type={TextBoxType.TEXT}
                             required={true}
                             placeholder={"Insertar nombre"}
                             hideWarningIcon={this.props.hasInvalidEmailOrPassword}/>

                    <TextBoxPhone defaultText="" label={"Teléfono a recargar"}
                                  style={styles.phoneTextBox}
                                  onChangeText={() => ""}
                                  hasError={this.state.showErrors}
                                  labelError={this.state.emailError}
                                  type={TextBoxType.NUMBER}
                                  required={true}
                                  placeholder={"53999999"}
                                  hideWarningIcon={this.props.hasInvalidEmailOrPassword}/>

                    <Dropdown style={styles.inputMarginBottom} heightRatio={4}
                              text={this.state.amountSelected.value}
                              options={this.state.amounts}
                              onChange={this.onChangeAmount}
                              required={true}
                              hasError={this.state.showErrors}
                              labelError=""
                              label={"Monto a recargar"}/>

                    <Button value={"Recarga"} type={ButtonType.BLUE}
                            containerButtonStyle={styles.rechargeButton} diffWidthAnimated={-30}
                            onPress={() => ""}/>

                    <Button value={"Recargas frecuentes"} type={ButtonType.GREEN}
                            containerButtonStyle={styles.rechargeFrecButton} diffWidthAnimated={-30}
                            onPress={this.props.onPressRechargesFrequent}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerMain: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        marginTop: 5
    },
    container: {
        paddingRight: 12,
        paddingLeft: 12
    },
    logo: {
        height: 120
    },
    iso: {
        position: 'absolute',
        width: 24,
        height: 24,
        borderRadius: 7
    },
    nameTextBox: {
        marginTop: 30,
    },
    phoneTextBox: {
        marginBottom: 100,
        marginTop: 15,
    },
    rechargeButton: {},
    rechargeFrecButton: {
        marginTop: 10
    },
    labelSeparator: {
        backgroundColor: COLORS.WHITE,
        marginTop: 24,
        marginBottom: 12
    },
    labelSeparatorText: {
        color: COLORS.TEXT_COLOR,
        paddingTop: 14,
        paddingBottom: 14,
        fontSize: 13,
        lineHeight: 16,
        alignSelf: 'center',
        letterSpacing: 2,
        fontWeight: 'bold'
    },
    navBar: {
        backgroundColor: COLORS.LIGHT_BG,
    },
    navBarWhite: {
        backgroundColor: COLORS.WHITE,
    },
    inputMarginBottom: {
        marginBottom: 26
    }
});


RechargeCellView.propTypes = {
    onOpenMenu: PropTypes.func,
    onPressRechargesFrequent: PropTypes.func,
};

export default RechargeCellView;