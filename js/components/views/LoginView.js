import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Spinner from '../generics/Spinner'
import TextBox from '../generics/TextBox';
import TextBoxType from "../generics/TextBoxType";
import Button from "../generics/Button";
import * as ButtonType from "../generics/ButtonType";
import * as ValidateConstant from "../../constants/ValidateConstant"
import {COLORS} from '../../constants/StylesConstants'
import {GLOBAL, LOGIN} from '../../constants/LabelConstants'
import {StyleSheet, Text, View, TouchableOpacity, Dimensions, Animated} from 'react-native';

class LoginView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: this.props.email,
            password: null,
            emailError: ValidateConstant.ERROR_REQUIRED,
            passwordError: ValidateConstant.ERROR_REQUIRED,
            showErrors: false,
            shapeARotate: new Animated.Value(0),
            shapeBRotate: new Animated.Value(0),
            shapeCRotate: new Animated.Value(0),
            shapeAOpacity: new Animated.Value(0),
            shapeBOpacity: new Animated.Value(0),
            shapeCOpacity: new Animated.Value(0),
            scaleIso: new Animated.Value(180),
            logoOpacity: new Animated.Value(0),
            titleOpacity: new Animated.Value(0),
            hideFooter: false
        };

        this.onChangeTextEmail = this.onChangeTextEmail.bind(this);
        this.onChangeTextPassword = this.onChangeTextPassword.bind(this);
        this.validate = this.validate.bind(this);
        this.sendData = this.sendData.bind(this);
        this.renderLogo = this.renderLogo.bind(this);
    }

    onChangeTextEmail(email, emailError) {
        this.setState({email, emailError});
        this.props.onChangeEmail();
    }

    onChangeTextPassword(password, passwordError) {
        this.setState({password, passwordError});
        this.props.onChangePassword();
    }

    sendData() {
        // if (this.validate()) {
        //     this.props.onPressLogin(this.state.email, this.state.password)();
        // } else {
        //     this.setState({showErrors: true});
        // }
        this.props.onPressLogin(this.state.email, this.state.password)();
    }

    validate() {
        if (this.state.emailError === null && this.state.passwordError === null) {
            return true;
        } else {
            return false;
        }
    }

    renderLogo() {
        return (<View style={styles.header}>
            <Animated.Image resizeMode="contain" source={require('../../../assets/images/logoR.jpg')}
                            style={[styles.logo, {
                                opacity: this.state.logoOpacity,
                            }]}/>
        </View>);
    }

    render() {

        let logo = this. renderLogo();
        return (
            <View style={styles.containerMain}>

                <View style={styles.container}>

                    {logo}

                    <View>
                        <Text style={[styles.initSession]}>
                            {LOGIN.START_SESSION}
                        </Text>
                    </View>

                    <TextBox defaultText={this.state.email} label={GLOBAL.EMAIL}
                             style={styles.emailTextBox}
                             onChangeText={this.onChangeTextEmail}
                             hasError={this.state.showErrors}
                             labelError={this.state.emailError}
                             type={TextBoxType.EMAIL}
                             required={true}
                             hideWarningIcon={this.props.hasInvalidEmailOrPassword}/>

                    <TextBox defaultText={this.state.password} label={GLOBAL.PASSWORD}
                             type={TextBoxType.PASSWORD} style={styles.passwordTextBox}
                             onChangeText={this.onChangeTextPassword}
                             hasError={this.state.showErrors}
                             labelError={this.state.passwordError}
                             required={true}/>

                    <TouchableOpacity style={[styles.forgetPassword]} onPress={this.props.onPressForgotPassword}>
                        <Text style={[styles.forgetPasswordText]}>{LOGIN.FORGET_PASSWORD}</Text>
                    </TouchableOpacity>

                    <Button value={LOGIN.BUTTON_START_SESSION} type={ButtonType.BLUE}
                            containerButtonStyle={styles.loginBtnContainer}
                            onPress={this.sendData}/>

                    <TouchableOpacity style={[styles.forgetPassword]} onPress={this.props.onPressForgotPassword}>
                        <Text style={[styles.forgetPasswordText,{textDecorationLine:'underline'}]}>{LOGIN.NEW_ACCOUNT}</Text>
                    </TouchableOpacity>


                </View>

                <Spinner visible={this.props.isVisibleLoadingLogin} textStyle={styles.spinnerTextStyle}/>

            </View>
        );
    }
}

let {height, width} = Dimensions.get('window');
const styles = StyleSheet.create({
    containerMain: {
        flex: 1
    },
    containerKeyboard: {
        flex: 1,
        borderWidth: 1
    },
    container: {
        backgroundColor: COLORS.WHITE,
        marginTop: 22,
        paddingLeft: 24,
        paddingRight: 24,
        flex: 3.44,
    },
    imageLogo: {
        width: 70,
        height: 24,
        alignSelf: 'center'
    },
    initSession: {
        fontSize: 30,
        color: COLORS.TEXT_COLOR,
        alignSelf: 'flex-start',
    },
    buttonText: {
        color: COLORS.WHITE,
        alignSelf: "center",
    },
    forgetPassword: {
        marginTop: 24,
    },
    forgetPasswordText: {
        color: COLORS.BLUE_COLOR_DARK,
        alignSelf: "center",
    },
    animateArea: {
        flex: 1,
        position: 'relative',
        width: width,
        justifyContent: 'flex-end'
    },
    imageFooter: {
        flex: 1, width: null, height: null,
    },
    emailTextBox: {
        marginTop: 24
    },
    passwordTextBox: {
        marginTop: 24
    },
    loginBtnContainer: {
        marginTop: 24
    },
    spinnerTextStyle: {
        color: COLORS.WHITE
    },
    shape: {
        position: 'absolute',
        left: -50,
        bottom: -50,
        width: (width + 100),
        alignSelf: 'flex-end',
        height: ((width + 150) * 381) / 938
    },
    header: {
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 2,
    },
    logo: {
        width: 160,
        alignSelf: 'center',
        height: 24,
    },
    iso: {
        backgroundColor: COLORS.PRIMARY_COLOR,
        position: 'absolute',
        top: 27,
        left: width / 2 - 47,
        width: 6,
        height: 6,
        borderRadius: 3
    },
});

LoginView.propTypes = {
    email: PropTypes.string,
    isVisibleLoadingLogin: PropTypes.bool,
    onPressLogin: PropTypes.func.isRequired,
    onPressForgotPassword: PropTypes.func.isRequired,
    hasInvalidPassword: PropTypes.bool,
    hasInvalidEmail: PropTypes.bool,
    onChangeEmail: PropTypes.func.isRequired,
    onChangePassword: PropTypes.func.isRequired,
};

export default LoginView