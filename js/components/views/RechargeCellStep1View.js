import React, {Component} from 'react';
import {View, Text, StyleSheet,} from 'react-native';
import {GLOBAL, LOGIN} from "../../constants/LabelConstants";
import Button from "../generics/Button";
import * as ButtonType from "../generics/ButtonType";
import {COLORS, ICON} from '../../constants/StylesConstants'
import NavigationBar from '../generics/navbar/NavigationBar'
import NavBarTitle from '../generics/NavBarTitle'
import Check from '../generics/Check'
import NavigatorBarButton from "../generics/NavigatorBarButton"
import RechargeCellFrequentItem from "../generics/recharge/RechargeCellFrequentItem"
import PropTypes from 'prop-types';
import StepIndicator from 'react-native-step-indicator';
import {LABELS, CUSTOM_STYLES} from '../../constants/WizardConstants'

class RechargeCellStep1View extends Component {

    constructor(props) {
        super(props);
        this.state = {
            termsAccept: false,
        };
        this.onChangeChecked = this.onChangeChecked.bind(this)
    }

    onChangeChecked(checked) {
        this.setState({termsAccept: checked})
    }

    render() {
        return (
            <View style={styles.containerMain}>

                <NavigationBar
                    containerStyle={styles.navBarWhite}
                    title={<NavBarTitle title={"Número a recargar"} showNavTitle={true} isAnimated={false}/>}
                    leftButton={<NavigatorBarButton title={GLOBAL.BACK} icon={ICON.BACK}
                                                    onPress={this.props.onPressBack}/>}
                />

                <View style={styles.stepIndicator}>
                    <StepIndicator
                        stepCount={4}
                        customStyles={CUSTOM_STYLES}
                        currentPosition={0}
                        labels={LABELS}
                    />
                </View>

                <RechargeCellFrequentItem
                    showRechargeButton={false}
                    disabledDropdown={true}
                />

                <View style={styles.containerBorder}>
                    <Text style={styles.labelTitle}>{"Verificar los datos a recargar".toUpperCase()}</Text>
                    <Check style={styles.checkStyle}
                           checked={this.state.termsAccept}
                           onChangeChecked={this.onChangeChecked}
                           text={"He revisado los números y entiendo que una vez recargados NO SE ACEPTAN RECLAMACIONES"}/>

                </View>
                <Button value={"Añador otro número"} type={ButtonType.BLUE}
                        containerButtonStyle={styles.otherNumberButton} diffWidthAnimated={-30}
                        onPress={() => ""}/>

                <Button value={"Terminar y pagar"} type={ButtonType.GREEN}
                        containerButtonStyle={styles.finishButton} diffWidthAnimated={-30}
                        onPress={() => ""}/>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerMain: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        paddingLeft: 12,
        paddingRight: 12,
    },
    otherNumberButton: {
        marginTop: 24
    },
    finishButton: {
        marginTop: 10
    },
    labelTitle: {
        fontSize: 13,
        lineHeight: 16,
        letterSpacing: 1.5,
        color: COLORS.BLUE_COLOR_DARK,
        fontWeight: 'bold'
    },
    checkStyle: {
        marginTop: 14
    },
    stepIndicator: {
        marginTop: 24
    },
    containerBorder: {
        padding: 7,
        borderWidth: 1,
        borderColor: COLORS.BORDER_COLOR,
        borderRadius: 4,
        marginTop: 10,
        paddingRight: 12,
        paddingLeft: 12
    }

});


RechargeCellStep1View.propTypes = {
    onPressBack: PropTypes.func,
};

export default RechargeCellStep1View;