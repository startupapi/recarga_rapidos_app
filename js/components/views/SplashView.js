import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    Image,
    Animated
} from 'react-native';
let dimensions = Dimensions.get('window');
let windowWidth = dimensions.width;

class SplashView extends Component {
 constructor(props){
        super(props);
        this.state = {
            fadeAnim: new Animated.Value(0),
            widthAnim: new Animated.Value(0),
            rightIso: new Animated.Value(0),
            opacityIso: new Animated.Value(0),
            scaleIso: new Animated.Value(1),
            backgroundIso: new Animated.Value(0),
        };
    }

    componentDidMount(){
        Animated.sequence([
            Animated.parallel([          // after decay, in parallel:
                Animated.timing(this.state.fadeAnim, {
                    toValue: 1,
                    duration: 300,
                }),
                Animated.timing(this.state.widthAnim, {   // and twirl
                    toValue: 250,
                    duration: 400,
                }),
            ]),

            Animated.timing(this.state.widthAnim, {   // and twirl
                toValue: 150,
                duration: 400,
            }),

            Animated.parallel([          // after decay, in parallel:
                Animated.timing(this.state.opacityIso, {
                    toValue: 1,
                    duration: 200,
                }),
                Animated.timing(this.state.rightIso, {   // and twirl
                    toValue: 160,
                    duration: 400,
                }),
            ]),
            Animated.timing(this.state.scaleIso, {
                toValue: 2,
                duration: 180,
            }),

            Animated.timing(this.state.scaleIso, {
                toValue: 1,
                duration: 180,
            }),

            Animated.parallel([
                Animated.timing(this.state.backgroundIso, {
                    toValue: 150,
                    duration: 400,
                }),
                Animated.timing(this.state.scaleIso, {
                    toValue: 100,
                    duration: 600,
                    delay: 600,
                }),

            ]),

        ]).start((e)=>{
            this.props.onSplashEnd()
        });
    }

    render() {
        const interpolateBackground = this.state.backgroundIso.interpolate({
            inputRange : [0, 150],
            outputRange: ['rgba(256,256,256, 0)', 'rgba(120, 89, 171, 1)']
        })
        const animBackg = {backgroundColor: interpolateBackground}
        return (
            <View style={styles.container}>
                <Animated.Image resizeMode="contain" style={[styles.logo, {width: this.state.widthAnim, opacity: this.state.fadeAnim}]} source={require('../../../assets/images/logoR.jpg')}>
                </Animated.Image>
                <Animated.Image
                    source={require('../../../assets/images/logoR.jpg')}
                    style={[styles.iso, animBackg, {
                        right: this.state.rightIso,
                        opacity: this.state.opacityIso,
                        transform: [
                            { scale: this.state.scaleIso }
                        ]
                    }]}/>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    logo: {
        height: 120
    },
    iso: {
        position: 'absolute',
        width: 24,
        height: 24,
        borderRadius: 7
    }
});

export default SplashView;