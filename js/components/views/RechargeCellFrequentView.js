import React, {Component} from 'react';
import {
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import {GLOBAL, LOGIN} from "../../constants/LabelConstants";
import {FONTS, COLORS, ICON} from '../../constants/StylesConstants'
import NavigationBar from '../generics/navbar/NavigationBar'
import NavBarTitle from '../generics/NavBarTitle'
import NavigatorBarButton from "../generics/NavigatorBarButton"
import RechargeCellFrequentItem from "../generics/recharge/RechargeCellFrequentItem"
import PropTypes from 'prop-types';

class RechargeCellFrequentView extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.containerMain}>

                <NavigationBar
                    containerStyle={styles.navBarWhite}
                    title={<NavBarTitle title={"Recargas frecuentes"} showNavTitle={true} isAnimated={false}/>}
                    leftButton={<NavigatorBarButton title={GLOBAL.BACK} icon={ICON.BACK}
                                                    onPress={this.props.onPressBack}/>}
                />

                <RechargeCellFrequentItem onPressRecharge={this.props.onPressRecharge}/>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerMain: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        paddingLeft: 12,
        paddingRight: 12,
    },
});


RechargeCellFrequentView.propTypes = {
    onPressBack: PropTypes.func,
    onPressRecharge: PropTypes.func,
};

export default RechargeCellFrequentView;