import React, {Component} from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {GLOBAL, LOGIN} from "../../constants/LabelConstants";
import Button from "../generics/Button";
import * as ButtonType from "../generics/ButtonType";
import {COLORS, ICON} from '../../constants/StylesConstants'
import NavigationBar from '../generics/navbar/NavigationBar'
import NavBarTitle from '../generics/NavBarTitle'
import NavigatorBarButton from "../generics/NavigatorBarButton"
import RechargeCellFrequentPaymentItem from "../generics/recharge/RechargeCellFrequentPaymentItem"
import PropTypes from 'prop-types';
import StepIndicator from 'react-native-step-indicator';
import {LABELS, CUSTOM_STYLES} from '../../constants/WizardConstants'
import TextBoxType from "../generics/TextBoxType";
import TextBox from '../generics/TextBox';
import Radio from '../generics/Radio'
import Dropdown from '../generics/dropdown/Dropdown'
import Check from '../generics/Check'
import * as ValidateConstant from "../../constants/ValidateConstant";

class RechargeCellStep3View extends Component {

    constructor(props) {
        super(props);
        let amountSelected = {
            id: 20,
            value: "Visa termina 8900",
            selected: true
        };
        let expirationDateSelected = {
            id: 4,
            value: "4",
            selected: true
        };
        this.state = {
            termsRegisterMethod: true,
            termsNewMethod: false,
            amountSelected: amountSelected,
            expirationDateSelected: expirationDateSelected,
            amounts: [
                amountSelected,
                {
                    id: 50,
                    value: "Mastercard termina 6785",
                    selected: false
                }
            ],
            expirationDates: [
                {
                    id: 1,
                    value: "1",
                    selected: false
                },
                expirationDateSelected,
                {
                    id: 2,
                    value: "2",
                    selected: false
                },
                {
                    id: 3,
                    value: "3",
                    selected: false
                }
            ]
        };
        this.onChangeCheckedRegisterMethod = this.onChangeCheckedRegisterMethod.bind(this);
        this.onChangeCheckedNewMethod = this.onChangeCheckedNewMethod.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
    }

    onChangeAmount(item, error) {
        let amountSelected = this.state.amountSelected;
        let amounts = this.state.amounts.map(value => {
            if (value.id === item.id) {
                amountSelected = {
                    id: value.id,
                    value: value.value,
                    selected: true
                };
                return amountSelected;
            } else {
                return {
                    id: value.id,
                    value: value.value,
                    selected: false
                };
            }
        });
        this.setState({amounts: amounts, amountSelected: amountSelected})
    }

    onChangeCheckedRegisterMethod(checked) {
        this.setState({termsRegisterMethod: true, termsNewMethod: false})
    }

    onChangeCheckedNewMethod(checked) {
        this.setState({termsRegisterMethod: false, termsNewMethod: true})
    }

    render() {
        return (
            <View style={styles.containerMain}>

                <NavigationBar
                    containerStyle={styles.navBarWhite}
                    title={<NavBarTitle title={"Elejir método de pago"} showNavTitle={true} isAnimated={false}/>}
                    leftButton={<NavigatorBarButton title={GLOBAL.BACK} icon={ICON.BACK}
                                                    onPress={this.props.onPressBack}/>}
                />

                <ScrollView>
                    <View style={styles.stepIndicator}>
                        <StepIndicator
                            stepCount={4}
                            customStyles={CUSTOM_STYLES}
                            currentPosition={2}
                            labels={LABELS}
                        />
                    </View>


                    <View style={styles.containerBorder}>
                        <Radio style={styles.checkStyle}
                               checked={this.state.termsRegisterMethod}
                               onChangeChecked={this.onChangeCheckedRegisterMethod}
                               text={"Método de pago registrado"}/>

                        <Dropdown style={styles.inputMarginBottom} heightRatio={4}
                                  text={this.state.amountSelected.value}
                                  options={this.state.amounts}
                                  onChange={this.onChangeAmount}
                                  required={true}
                                  visible={this.state.termsRegisterMethod}
                                  disabled={this.props.disabledDropdown}
                                  hasError={this.state.showErrors}
                                  labelError=""
                                  label={"Monto a recargar"}/>

                    </View>


                    <View style={styles.containerBorder}>
                        <Radio style={styles.checkStyle}
                               checked={this.state.termsNewMethod}
                               onChangeChecked={this.onChangeCheckedNewMethod}
                               text={"Nuevo método de pago"}/>

                        <TextBox defaultText="" label={"Nombre en la tarjeta"}
                                 style={[styles.nameTextBox, styles.inputMarginBottom]}
                                 onChangeText={() => ""}
                                 hasError={this.state.showErrors}
                                 labelError={this.state.emailError}
                                 type={TextBoxType.TEXT}
                                 required={true}
                                 visible={this.state.termsNewMethod}
                                 placeholder={"Insertar nombre"}
                                 hideWarningIcon={true}/>

                        <TextBox defaultText="" label={"Número en la tarjeta"}
                                 style={[styles.nameTextBox, styles.inputMarginBottom]}
                                 onChangeText={() => ""}
                                 hasError={this.state.showErrors}
                                 labelError={this.state.emailError}
                                 type={TextBoxType.TEXT}
                                 required={true}
                                 visible={this.state.termsNewMethod}
                                 placeholder={"Insertar número"}
                                 hideWarningIcon={true}/>

                        <View style={styles.containerExpirationDate}>
                            <Dropdown style={[styles.inputMarginBottom, {flex: 1, marginRight: 2}]} heightRatio={4}
                                      text={this.state.expirationDateSelected.value}
                                      options={this.state.expirationDates}
                                      onChange={this.onChangeAmount}
                                      required={true}
                                      visible={this.state.termsNewMethod}
                                      disabled={this.props.disabledDropdown}
                                      hasError={this.state.showErrors}
                                      labelError=""
                                      label={"Día"}/>

                            <Dropdown style={[styles.inputMarginBottom, {flex: 1, marginLeft: 2}]} heightRatio={4}
                                      text={this.state.expirationDateSelected.value}
                                      options={this.state.expirationDates}
                                      onChange={this.onChangeAmount}
                                      required={true}
                                      visible={this.state.termsNewMethod}
                                      disabled={this.props.disabledDropdown}
                                      hasError={this.state.showErrors}
                                      labelError=""
                                      label={"Mes"}/>
                        </View>

                        <TextBox defaultText="" label={"CVV/CVV2"}
                                 style={[styles.nameTextBox, styles.inputMarginBottom]}
                                 onChangeText={() => ""}
                                 hasError={this.state.showErrors}
                                 labelError={this.state.emailError}
                                 type={TextBoxType.TEXT}
                                 required={true}
                                 visible={this.state.termsNewMethod}
                                 placeholder={"Insertar CVV/CVV2"}
                                 hideWarningIcon={true}/>


                        <Check style={styles.checkStyle}
                               checked={true}
                               onChangeChecked={this.onChangeChecked}
                               visible={this.state.termsNewMethod}
                               text={"Guardar tarjeta"}/>
                    </View>

                    <View style={styles.containerTotal}>
                        <View style={styles.containerTotalBorder}/>
                        <View style={styles.containerTotalValue}>
                            <Text
                                style={[styles.inputTotal, styles.inputTotalLabel]}>{"Total a pagar:".toUpperCase()}</Text>
                            <Text style={[styles.inputTotal, styles.inputTotalValue]}>{"$33.30"}</Text>
                        </View>
                    </View>

                    <Button value={"Continuar y pagar"} type={ButtonType.GREEN}
                            containerButtonStyle={styles.finishButton} diffWidthAnimated={-30}
                            onPress={() => ""}/>

                    <Button value={"Cancelar recarga"} type={ButtonType.FLAT}
                            containerButtonStyle={styles.cancelButton} diffWidthAnimated={-30}
                            onPress={() => ""}/>


                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerMain: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        paddingLeft: 12,
        paddingRight: 12,
    },
    cancelButton: {
        marginTop: 10,
        marginBottom: 15
    },
    finishButton: {
        marginTop: 10
    },
    labelTitle: {
        fontSize: 13,
        lineHeight: 16,
        letterSpacing: 1.5,
        color: COLORS.BLUE_COLOR_DARK,
        fontWeight: 'bold'
    },
    checkStyle: {
        marginTop: 3
    },
    stepIndicator: {
        marginTop: 7
    },
    containerBorder: {
        padding: 7,
        borderWidth: 1,
        borderColor: COLORS.BLUE_COLOR_DARK,
        borderRadius: 4,
        marginTop: 10,
        paddingRight: 12,
        paddingLeft: 12
    },
    labelTitleCupon: {
        fontSize: 16,
        marginTop: 10,
        color: COLORS.TEXT_LIGHTEEN_4
    },
    inputCupon: {
        marginTop: 10,
    },
    containerTotal: {
        marginTop: 10,
        flexDirection: 'row',
    },
    containerTotalValue: {
        flex: 1,
        backgroundColor: COLORS.BLUE_COLOR_LIGHTEEN,
        height: 50,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 13
    },
    containerTotalBorder: {
        backgroundColor: COLORS.BLUE_COLOR_BORDER,
        height: 50,
        width: 10,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4
    },
    inputTotal: {
        color: COLORS.WHITE,
        fontSize: 20
    },
    inputTotalLabel: {
        fontSize: 16,
        marginLeft: 10
    },
    inputTotalValue: {
        fontWeight: 'bold',
        marginRight: 10
    },
    inputMarginBottom: {
        marginTop: 10
    },
    containerExpirationDate: {
        flexDirection: 'row',
    }

});


RechargeCellStep3View.propTypes = {
    onPressBack: PropTypes.func,
};

export default RechargeCellStep3View;