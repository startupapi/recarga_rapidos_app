import React, {Component} from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {GLOBAL, LOGIN} from "../../constants/LabelConstants";
import Button from "../generics/Button";
import * as ButtonType from "../generics/ButtonType";
import {COLORS, ICON} from '../../constants/StylesConstants'
import NavigationBar from '../generics/navbar/NavigationBar'
import NavBarTitle from '../generics/NavBarTitle'
import NavigatorBarButton from "../generics/NavigatorBarButton"
import RechargeCellFrequentPaymentItem from "../generics/recharge/RechargeCellFrequentPaymentItem"
import PropTypes from 'prop-types';
import StepIndicator from 'react-native-step-indicator';
import {LABELS, CUSTOM_STYLES} from '../../constants/WizardConstants'
import TextBoxType from "../generics/TextBoxType";
import TextBox from '../generics/TextBox';
import Check from '../generics/Check'

class RechargeCellStep2View extends Component {

    constructor(props) {
        super(props);
        this.state = {
            termsAccept: false,
        };
        this.onChangeChecked = this.onChangeChecked.bind(this)
    }

    onChangeChecked(checked) {
        this.setState({termsAccept: checked})
    }

    render() {
        return (
            <View style={styles.containerMain}>

                <NavigationBar
                    containerStyle={styles.navBarWhite}
                    title={<NavBarTitle title={"Número a recargar"} showNavTitle={true} isAnimated={false}/>}
                    leftButton={<NavigatorBarButton title={GLOBAL.BACK} icon={ICON.BACK}
                                                    onPress={this.props.onPressBack}/>}
                />

                <ScrollView>
                <View style={styles.stepIndicator}>
                    <StepIndicator
                        stepCount={4}
                        customStyles={CUSTOM_STYLES}
                        currentPosition={1}
                        labels={LABELS}
                    />
                </View>

                <RechargeCellFrequentPaymentItem text={"$21.80"}/>

                {/*<View style={styles.containerBorder}>*/}
                    {/*<Text style={styles.labelTitle}>{"Usar un cupón promocional".toUpperCase()}</Text>*/}
                    {/*<Text style={styles.labelTitleCupon}>Introduzca el cupón de regalo para aplicar el descuento a su*/}
                        {/*pago.</Text>*/}

                    <TextBox
                        defaultText={this.state.password} label={"cupón"}
                        type={TextBoxType.CUPON}
                        style={styles.inputCupon}
                        onChangeText={() => ""}
                        hasError={this.state.showErrors}
                        labelError={this.state.passwordError}
                        required={false}
                        borderColorStyle={COLORS.BLUE_COLOR_DARK}
                        labelColorStyle={COLORS.BLUE_COLOR_DARK}
                        placeholder={"¿Tienes un cupón?"}
                        onPressShowValidateCupon={() => alert('validar')}/>
                {/*</View>*/}

                <View style={styles.containerTotal}>
                    <View style={styles.containerTotalBorder}/>
                    <View style={styles.containerTotalValue}>
                        <Text
                            style={[styles.inputTotal, styles.inputTotalLabel]}>{"Total a pagar:".toUpperCase()}</Text>
                        <Text style={[styles.inputTotal, styles.inputTotalValue]}>{"$33.30"}</Text>
                    </View>
                </View>

                <Check style={styles.checkStyle}
                       checked={this.state.termsAccept}
                       onChangeChecked={this.onChangeChecked}
                       text={"Pre-reservar recarga."}/>

                <Button value={"Proceder con el pago"} type={ButtonType.GREEN}
                        containerButtonStyle={styles.finishButton} diffWidthAnimated={-30}
                        onPress={() => ""}/>

                <Button value={"Cancelar recarga"} type={ButtonType.FLAT}
                        containerButtonStyle={styles.cancelButton} diffWidthAnimated={-30}
                        onPress={() => ""}/>



                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerMain: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        paddingLeft: 12,
        paddingRight: 12,
    },
    cancelButton: {
        marginTop: 10
    },
    finishButton: {
        marginTop: 10
    },
    labelTitle: {
        fontSize: 13,
        lineHeight: 16,
        letterSpacing: 1.5,
        color: COLORS.BLUE_COLOR_DARK,
        fontWeight: 'bold'
    },
    checkStyle: {
        marginTop: 10
    },
    stepIndicator: {
        marginTop: 7
    },
    containerBorder: {
        padding: 7,
        borderWidth: 1,
        borderColor: COLORS.BLUE_COLOR_DARK,
        borderRadius: 4,
        marginTop: 10,
        paddingRight: 12,
        paddingLeft: 12
    },
    labelTitleCupon: {
        fontSize: 16,
        marginTop: 10,
        color: COLORS.TEXT_LIGHTEEN_4
    },
    inputCupon: {
        marginTop: 10,
    },
    containerTotal: {
        marginTop: 10,
        flexDirection: 'row',
    },
    containerTotalValue: {
        flex: 1,
        backgroundColor: COLORS.BLUE_COLOR_LIGHTEEN,
        height: 50,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 13
    },
    containerTotalBorder: {
        backgroundColor: COLORS.BLUE_COLOR_BORDER,
        height: 50,
        width: 10,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4
    },
    inputTotal: {
        color: COLORS.WHITE,
        fontSize: 20
    },
    inputTotalLabel: {
        fontSize: 16,
        marginLeft: 10
    },
    inputTotalValue: {
        fontWeight: 'bold',
        marginRight: 10
    }

});


RechargeCellStep2View.propTypes = {
    onPressBack: PropTypes.func,
};

export default RechargeCellStep2View;